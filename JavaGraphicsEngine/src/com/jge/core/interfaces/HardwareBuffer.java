package com.jge.core.interfaces;

import java.nio.FloatBuffer;

import com.jge.core.exceptions.JavaGraphicsException;

public abstract class HardwareBuffer {
    protected HardwareBuffer.Usage usage;
    protected long sizeInBytes;

    protected HardwareBuffer(HardwareBuffer.Usage usage) {
        super();
        this.usage = usage;
    }

    public HardwareBuffer.Usage getUsage() {
        return usage;
    }

    public long getSizeInBytes() {
        return sizeInBytes;
    }

    abstract public byte[] readData(int offset, int length) throws JavaGraphicsException;
    abstract public void writeData(int offset, int length, float data[]) throws JavaGraphicsException;

    public static enum Usage {
        HBU_STATIC,
        HBU_STATIC_WRITE_ONLY,
        HBU_DYNAMIC,
        HBU_DYNAMIC_WRITE_ONLY,
        HBU_DYNAMIC_WRITE_ONLY_DISCARDABLE
    }
}
