package com.jge.core.interfaces;

import com.jge.core.beans.SceneNode;
import com.jge.core.exceptions.JavaGraphicsException;

public interface Moveable extends Naming {
    void setVisible(boolean visible) throws JavaGraphicsException;
    boolean isVisible() throws JavaGraphicsException;
    boolean isAttached() throws JavaGraphicsException;
    SceneNode getSceneNode() throws JavaGraphicsException;
    void setSceneNode(SceneNode node) throws JavaGraphicsException;
    void updateRenderQueue(RenderQueue queue) throws JavaGraphicsException;
}
