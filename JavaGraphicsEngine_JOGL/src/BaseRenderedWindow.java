import java.util.logging.Level;
import java.util.logging.Logger;


import com.jge.core.Root;
import com.jge.core.exceptions.JavaGraphicsException;
import com.jge.core.interfaces.RenderSystem;
import com.jge.core.interfaces.RenderWindowListener;
import com.jge.jogl.core.JgeInitialize;


public abstract class BaseRenderedWindow implements RenderWindowListener  {
    private Root root;

    protected BaseRenderedWindow(String title, int width, int height) throws JavaGraphicsException {
        super();
        Logger.getGlobal().setLevel(Level.ALL);
        root = JgeInitialize.initialize();

        RenderSystem.getInstance().initialize(title, width, height); 
        RenderSystem.getInstance().getRenderWindow().setRenderWindowListener(this);
    }
}
