package com.jge.jogl.core.objects;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.media.opengl.GL2;
import javax.media.opengl.GLException;
import javax.media.opengl.GLProfile;

import com.jge.core.beans.GroupLocation;
import com.jge.core.beans.Resource;
import com.jge.core.beans.ResourceGroup;
import com.jge.core.beans.Task;
import com.jge.core.exceptions.JavaGraphicsException;
import com.jge.core.managers.ResourceGroupManager;
import com.jge.core.managers.ResourceManager;
import com.jge.core.managers.TextureManager;
import com.jge.core.objects.Texture;
import com.jge.jogl.core.impl.RenderSystemOpenGL;
import com.jogamp.opengl.util.texture.awt.AWTTextureIO;

public class TextureOpenGL extends Texture{
    private Image image;
    private com.jogamp.opengl.util.texture.Texture texture;
    private File file;

    public TextureOpenGL(ResourceManager creator, String name,
            String groupName) throws JavaGraphicsException {
        super(creator, name, groupName);

        ResourceGroup group = ResourceGroupManager.getInstance().getResourceGroup(getGroupName());
        file = group.getFile(name);
        if (file == null) {
            // TODO JavaGraphicsException, фатальный, невозможно найти ресурс по name в указанных путях
        }
    }

    @Override
    public void preparing() throws JavaGraphicsException {
        try {
            image = ImageIO.read(file);
        } catch (IOException e) {
            throw new JavaGraphicsException(e);
        } finally {
            System.out.printf("Texture %s:%s was loading\n", getName(), file.getAbsolutePath());
        } 
    }

    @Override
    public void loading() throws JavaGraphicsException {
        RenderSystemOpenGL rs = ((RenderSystemOpenGL) RenderSystemOpenGL.getInstance());
        try { 
            GL2 gl = rs.getGL2();
            BufferedImage bi = (BufferedImage)image;

            texture = AWTTextureIO.newTexture(rs.getGLProfile(), bi, true);
        } catch (GLException e) {
            throw new JavaGraphicsException(e);
        } finally {
            System.out.printf("Texture %s:%s loaded\n", getName(), file.getAbsolutePath());
        }
    }

    public com.jogamp.opengl.util.texture.Texture getOpenGLTexture() {
        return texture;
    }

    @Override
    public void loadImpl() throws JavaGraphicsException {
        loading();
    }

    @Override
    public void unloadImpl() throws JavaGraphicsException {
        RenderSystemOpenGL rs = ((RenderSystemOpenGL) RenderSystemOpenGL.getInstance());
        GL2 gl = rs.getGL2();

        texture.destroy(gl);
    }

    @Override
    protected void prepareImpl() throws JavaGraphicsException {
        preparing();
    }	
}