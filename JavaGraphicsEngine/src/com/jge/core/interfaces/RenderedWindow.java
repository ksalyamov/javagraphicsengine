package com.jge.core.interfaces;

import java.awt.Component;

import com.jge.core.exceptions.JavaGraphicsException;
import com.jge.core.managers.SceneManager;

public abstract class RenderedWindow {
    protected SceneManager sceneManager;
    protected RenderSystem renderSystem;
    protected RenderedWindowListener rwListener;

    protected RenderedWindow(SceneManager sceneManager,
            RenderSystem renderSystem) {
        super();
        this.sceneManager = sceneManager;
        this.renderSystem = renderSystem;
    }

    public RenderedWindowListener getRenderedWindowListener() {
        return rwListener;
    }

    public void setRenderedWindowListener(RenderedWindowListener rwListener) {
        this.rwListener = rwListener;
    }

    abstract public Component getContentFrame();
}

