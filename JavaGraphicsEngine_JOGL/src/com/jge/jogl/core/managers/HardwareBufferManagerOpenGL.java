package com.jge.jogl.core.managers;

import com.jge.core.beans.VertexDeclaration;
import com.jge.core.exceptions.JavaGraphicsException;
import com.jge.core.interfaces.HardwareBuffer.Usage;
import com.jge.core.interfaces.HardwareVertexBuffer;
import com.jge.core.managers.HardwareBufferManager;
import com.jge.jogl.beans.VertexDeclarationOpenGL;
import com.jge.jogl.core.impl.HardwareVertexBufferOpenGL;
import com.jge.jogl.core.impl.RenderSystemOpenGL;
import com.jge.jogl.core.impl.RenderWindowOpenGL;

public class HardwareBufferManagerOpenGL extends HardwareBufferManager {
    private RenderSystemOpenGL renderSystem;

    public HardwareBufferManagerOpenGL(RenderSystemOpenGL renderSystem) throws JavaGraphicsException {
        super();
        HardwareBufferManager.setSingleton(this);
        this.renderSystem = renderSystem;
    }

    @Override
    protected HardwareVertexBuffer createVertexBufferImpl(int vertexSize,
            int numVertices, Usage usage) throws JavaGraphicsException {
        return new HardwareVertexBufferOpenGL(renderSystem, this, vertexSize, numVertices, usage);
    }

    @Override
    protected VertexDeclaration createVertexDeclarationImpl()
            throws JavaGraphicsException {
        return new VertexDeclarationOpenGL();
    }

}
