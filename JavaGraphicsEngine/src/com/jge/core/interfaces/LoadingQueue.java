package com.jge.core.interfaces;

import com.jge.core.beans.Task;
import com.jge.core.exceptions.JavaGraphicsException;

public interface LoadingQueue {
    void addTask(Task task);
    void removeTask(Task task);
    void loading() throws JavaGraphicsException;

    Task getTaskAndRemove();
    Task getTask();
    Task getTaskCreated();
    int size();
    boolean isEmtpy();

}
