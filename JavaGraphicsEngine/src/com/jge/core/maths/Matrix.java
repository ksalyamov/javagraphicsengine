package com.jge.core.maths;

public class Matrix {
    private float m[];

    private static final float[] IDENTITY = { 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1,
        0, 0, 0, 0, 1 };

    public Matrix() {
        super();
        this.m = new float[16];
    }

    public float[] getData() {
        return m;
    }

    public void setData(float[] m) {
        this.m = m;
    }

    /**
     * Превращает текущую матрицу в единичную
     */
    public void identity() {
        System.arraycopy(IDENTITY, 0, m, 0, m.length);
    }

    /**
     * Транспонирует текущую матринцу
     */
    public void transpose() {
        float transponse[] = new float[16];
        int count = 0;
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 16; j += 4) {
                transponse[count++] = m[i + j];
            }
        }
        System.arraycopy(transponse, 0, m, 0, m.length);
    }

    /**
     * Создает единичную матрицу и Устанавливает двухмерное координаты (x,y)
     * 
     * @param position
     *            положение
     */
    public void translation(Vector2d position) {
        identity();
        setPosition(position);
    }

    /**
     * Устанавливает у текущей матрицыкоординаты (x,y)
     * @param position положение
     */
    public void setPosition(Vector2d position) {
        m[12] = position.getX();
        m[13] = position.getY();
        m[14] = 0.0f;
    }

    /**
     * Получает двухмерные координаты (x,y)
     * 
     * @return
     */
    public Vector2d getPosition() {
        float x, y;
        x = m[12];
        y = m[13];
        Vector2d vector = new Vector2d();
        vector.setX(x);
        vector.setY(y);
        return vector;
    }

    /**
     * Вращает текущую матрицу по оси X
     * 
     * @param angle
     *            угол задается в градусах
     */
    public void rotateByX(float angle) {
        identity();

        double radian = Math.toRadians(angle);
        float cos = (float) Math.cos(radian);
        float sin = (float) Math.sin(radian);

        m[5] = cos;
        m[6] = -sin;
        m[9] = sin;
        m[10] = cos;
    }

    /**
     * Вращает текущую матрицу по оси Y
     * 
     * @param angle
     *            угол задается в градусах
     */
    public void rotateByY(float angle) {
        identity();

        double radian = Math.toRadians(angle);
        float cos = (float) Math.cos(radian);
        float sin = (float) Math.sin(radian);

        m[0] = cos;
        m[2] = -sin;
        m[8] = sin;
        m[9] = cos;
    }

    /**
     * Вращает текущую матрицу по оси Z
     * 
     * @param angle
     *            угол задается в градусах
     */
    public void rotateByZ(float angle) {
        identity();

        double radian = Math.toRadians(angle);
        float cos = (float) Math.cos(radian);
        float sin = (float) Math.sin(radian);

        m[0] = cos;
        m[1] = -sin;
        m[4] = sin;
        m[5] = cos;
    }

    /**
     * Вращает текущую матрицу в 2D-координатах. Фактически это всего лишь
     * вращение по оси Z.
     * 
     * @param angle
     *            Угол. Задается в градусах.
     */
    public void rotateIn2dWorld(float angle) {
        rotateByZ(angle);
    }

    /**
     * Умножает текущую матрицу на matrix и возвращает результат перемножения.
     * Результат <b>не заносится</b> в текущую матрицу.
     * 
     * @param matrix
     *            Матрица перемножения
     * @return Матрица. Результат перемножения.
     */
    public Matrix multiply(Matrix matrix) {
        float[] secondMatrix = matrix.getData();
        float[] result = new float[16];
        float temp = 0;

        for (int j = 0; j < 16; j += 4) {
            for (int i = 0; i < 4; i++) {
                for (int z = 0; z < 4; z++) {
                    temp = temp + m[j + z] * secondMatrix[i + 4 * z];
                }

                result[j + i] = temp;
                temp = 0;
            }
        }

        Matrix resultMatrix = new Matrix();
        resultMatrix.setData(result);

        return resultMatrix;
    }


    /**
     * Приравнивает matrix к текущей матрице
     * 
     * @param matrix
     *            Матрица
     */
    public void setMatrix(Matrix matrix) {
        System.arraycopy(matrix.getData(), 0, m, 0, m.length);
    }

    public static Matrix getTranspose() {
        Matrix matrix = new Matrix();
        matrix.transpose();
        return matrix;
    }

    public static Matrix getIdentity() {
        Matrix matrix = new Matrix();
        matrix.identity();
        return matrix;
    }
}
