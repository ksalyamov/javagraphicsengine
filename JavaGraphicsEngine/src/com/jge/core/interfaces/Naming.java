package com.jge.core.interfaces;

import com.jge.core.exceptions.JavaGraphicsException;

public interface Naming {
    void setName(String name) throws JavaGraphicsException;
    String getName() throws JavaGraphicsException;
}
