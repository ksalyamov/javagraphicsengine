package com.jge.core.managers;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.jge.core.beans.GroupLocation;
import com.jge.core.beans.Resource;
import com.jge.core.beans.ResourceGroup;
import com.jge.core.beans.Task;
import com.jge.core.enums.ExceptionType;
import com.jge.core.enums.LoadingStatus;
import com.jge.core.exceptions.JavaGraphicsException;
import com.jge.core.interfaces.LoadingQueue;

public class ResourceGroupManager {
    private static ResourceGroupManager instance;

    private Map<String, ResourceGroup> resourceGroups;
    private Map<String, ResourceManager> resourceManagers;
    private LoadingQueue loadingQueue;

    public ResourceGroupManager(LoadingQueue loadingQueue) {
        super();
        this.loadingQueue = loadingQueue;
        this.resourceGroups = new ConcurrentHashMap<String, ResourceGroup>();
        this.resourceManagers = new HashMap<String, ResourceManager>();
        ResourceGroupManager.setInstance(this);
    }

    protected static void setInstance(ResourceGroupManager singleton) {
        synchronized (ResourceGroupManager.class) {
            ResourceGroupManager.instance = singleton;
        }
    }

    public static ResourceGroupManager getInstance() {
        return ResourceGroupManager.instance;
    }

    public void createResourceGroup(String name) throws JavaGraphicsException {
        if (getResourceGroup(name) != null) {
            throw new JavaGraphicsException(ExceptionType.DUPLICATED_ITEM, String.format("Group with name '%s' already created", name));
        }
        ResourceGroup group = new ResourceGroup();
        group.setName(name);

        resourceGroups.put(name, group);
    }

    public ResourceGroup getResourceGroup(String name) throws JavaGraphicsException {
        return resourceGroups.get(name);
    }

    /**
     * Добавляет все незагруженные ресурсы в группе
     * @param name Имя группы
     * @throws JavaGraphicsException
     */
    public void loadResourceGroup(String name) throws JavaGraphicsException {
        ResourceGroup group = getResourceGroup(name);
        if (group == null) {
            throw new JavaGraphicsException(ExceptionType.MISSED_DATA, String.format("Group with name '%s' doesn't exist", name));
        }

        // Добавляем все ресурсы из данной группы в очередь на загрузку
        // Добавляются только незагруженные
        List<Resource> resources = group.getResources();
        int size = resources.size();
        for (int i=0; i<size; i++) {
            if (resources.get(i).getStatus() == LoadingStatus.PREPARED) {
                resources.get(i).load();
            }
        }
    }

    public void prepareResourceGroup(String name) throws JavaGraphicsException {
        ResourceGroup group = getResourceGroup(name);
        if (group == null) {
            throw new JavaGraphicsException(ExceptionType.MISSED_DATA, String.format("Group with name '%s' doesn't exist", name));
        }

        // Добавляем все ресурсы из данной группы в очередь на загрузку
        // Добавляются только незагруженные
        List<Resource> resources = group.getResources();
        int size = resources.size();
        for (int i=0; i<size; i++) {
            Resource resource = resources.get(i);
            if (resource.getStatus() == LoadingStatus.NONE ) {
                resource.prepare();
            }
        }
    }

    public void unloadResourceGroup(String name) throws JavaGraphicsException {
        ResourceGroup group = getResourceGroup(name);
        if (group == null) {
            throw new JavaGraphicsException(ExceptionType.MISSED_DATA, String.format("Group with name '%s' doesn't exist", name));
        }

        // Добавляем все ресурсы из данной группы в очередь на загрузку
        // Добавляются только незагруженные
        List<Resource> resources = group.getResources();
        int size = resources.size();
        for (int i=0; i<size; i++) {
            if (resources.get(i).getStatus() == LoadingStatus.LOADED) {
                resources.get(i).unload();
            } else if (resources.get(i).getStatus() != LoadingStatus.UNLOADED) {
                // TODO Логирование, о том что ресурс не освобождён
            }
        }
    }

    public void addResourceToLoadingQueue(Resource resource) throws JavaGraphicsException {
        throw new JavaGraphicsException(ExceptionType.NOT_IMPLEMENTED, "Method is not implemented");
    }

    /**
     * Создает ресурс по группе с указанным именем
     * @param groupName Название группы
     * @param resName Название ресурса
     * @throws JavaGraphicsException
     */
    public void createResource(String groupName, String resName) throws JavaGraphicsException {
        ResourceManager manager = getResourceManager(groupName);
        manager.create(resName);
    }

    /**
     * Загружает все незагруженные ресурсы 
     * @throws JavaGraphicsException
     */
    public void loadAllResourceGroups() throws JavaGraphicsException {
        for (Map.Entry<String, ResourceGroup> entry : resourceGroups.entrySet()) {
            loadResourceGroup(entry.getKey());
        }
    }

    public void unloadAllResourceGroups() throws JavaGraphicsException {
        for (Map.Entry<String, ResourceGroup> entry : resourceGroups.entrySet()) {
            unloadResourceGroup(entry.getKey());
        }
    }

    public void prepareAllResourceGroups() throws JavaGraphicsException {
        for (Map.Entry<String, ResourceGroup> entry : resourceGroups.entrySet()) {
            prepareResourceGroup(entry.getKey());
        }
    }

    /**
     * Добавляет местоположение ресурсов в группу ресурсов
     * @param location Путь до ресурсов (Папка)
     * @param type Формат файлов для данной группы ресурсов
     * @param groupName Название группы ресурсов
     * @throws JavaGraphicsException
     */
    public void addResourceLocation(String location, String type, String groupName)  throws JavaGraphicsException {
        ResourceGroup group = getResourceGroup(groupName);
        if (group == null) {
            createResourceGroup(groupName);
            group = getResourceGroup(groupName);
        }

        GroupLocation groupLocation = new GroupLocation(location, type);
        group.getLocations().add(groupLocation);
    }

    public void notifyResourceCreated(Resource res) throws JavaGraphicsException {
        ResourceGroup group = getResourceGroup(res.getGroupName());
        if (group != null) {
            addCreatedResource(res, group);
        }
    }

    private void addCreatedResource(Resource res, ResourceGroup group) {
        group.getResources().add(res);
    }

    public LoadingQueue getLoadingQueue() {
        return loadingQueue;
    }

    /**
     * Регистрирует ResourceManager по указанной группе ресурсов
     * @param type Тип (группа) ResourceManager
     * @param manager Ссылка на ResourceManager
     */
    public void registerResourceManager(String type, ResourceManager manager) {
        resourceManagers.put(type, manager);
    }

    /**
     * Регистрирует ResourceManager по default-группе ресурсов
     * @param manager
     */
    public void registerResourceManager(ResourceManager manager) {
        resourceManagers.put(manager.getGroupName(), manager);
    }

    /**
     * Снимает с регистрации указанный ResourceManager
     * @param manager
     */
    public void unregisterResourceManager(ResourceManager manager) {
        resourceManagers.remove(manager.getGroupName());
    }

    public ResourceManager getResourceManager(String type) {
        return resourceManagers.get(type);
    }

    /**
     * Возвращает коллекцию всех зарегистрированных ResourceManager
     * @return
     */
    public Collection<ResourceManager> getAllResourceManagers() {
        return resourceManagers.values();
    }


}	
