package com.jge.core.managers;

import java.io.File;

import com.jge.core.beans.Resource;
import com.jge.core.beans.ResourceGroup;
import com.jge.core.beans.Task;
import com.jge.core.enums.ExceptionType;
import com.jge.core.exceptions.JavaGraphicsException;

public abstract class ResourceManager {
    private String groupName;
    private Pool poolManager;

    /**
     * Конструктор. Регистрирует ResourceManager и создает для него группу
     * ресурсов по умолчанию
     * @param group Название группы ресурсов по умолчанию.
     * @throws JavaGraphicsException
     */
    protected ResourceManager(String group) throws JavaGraphicsException {
        super();
        this.groupName = group;
        ResourceGroupManager.getInstance().registerResourceManager(groupName, this);
        ResourceGroupManager.getInstance().createResourceGroup(groupName);
        this.poolManager = new Pool(this);
    }

    public String getGroupName() {
        return this.groupName;
    }

    abstract protected Resource createImpl(ResourceManager creator, String name, String group) throws JavaGraphicsException;

    /**
     * Создает ресурс по имени в указанной группе ресурсов.
     * Этот ресурс не добавляется в очередь на закачку.
     * 
     * @param name Имя ресурса
     * @param group Название группы ресурса
     * @return Ресурс
     * @throws JavaGraphicsException
     */
    public Resource create(String name, String group) throws JavaGraphicsException {
        Resource resource = createImpl(this, name, group);
        addImpl (resource);
        ResourceGroupManager.getInstance().notifyResourceCreated(resource);
        return resource;
    }

    /**
     * Создает ресурс по имени. Группа ресурсов по умолчанию
     * @param name Название ресурса
     * @return Ресурс
     * @throws JavaGraphicsException
     */
    public Resource create(String name) throws JavaGraphicsException {
        return create(name, getGroupName());
    }

    /**
     * Создает ресурс по имени в указанной группе ресурсов и 
     * добавляет его в очередь на загрузку
     * 
     * @param name Имя ресурса
     * @param group Название группы ресурса
     * @return Ресурс
     * @throws JavaGraphicsException
     */
    public Resource load(String name, String group) throws JavaGraphicsException {
        Resource resource = create(name, group);
        ResourceGroupManager.getInstance().getLoadingQueue().addTask(new Task(resource));
        return resource;
    }

    /**
     * Добавляет ресурс в группу ресурсов
     * @param resource Ссылка на ресурс
     * @throws JavaGraphicsException Дубль ресурса
     */
    protected void addImpl(Resource resource) throws JavaGraphicsException {
        for (Resource res : ResourceGroupManager.getInstance().getResourceGroup(resource.getGroupName()).getResources()) {
            if (res.getName().equals(resource.getName())) {
                // TODO нужно бросать JavaGraphicsException о дублировании ресурса, ошибка фатальная
            }
        }
        // Добавляем созданный ресурс в группу ресурсов
        ResourceGroupManager.getInstance().getResourceGroup(resource.getGroupName()).getResources().add(resource);
    }

    public Resource getByName(String name) throws JavaGraphicsException {
        return getByName(name, groupName);
    }

    public Resource getByName(String name, String groupName) throws JavaGraphicsException {
        // Оптимизировать
        for (Resource resource : ResourceGroupManager.getInstance().getResourceGroup(groupName).getResources()) {
            if (name.equals(resource.getName()) ) {
                return resource;
            }
        }
        return null;
    }

    // Пока никак не используется
    public static class Pool  {
        private ResourceManager manager;

        public Pool(ResourceManager manager) {
            super();
            this.manager = manager;
        }

        public void releaseItem(String name) throws JavaGraphicsException {
            releaseItem(manager.getByName(name));
        }

        public void releaseItem(Resource item) throws JavaGraphicsException {
            if (item != null && item.isLoaded()) {
                item.unload();
            }
        }

        public Resource createItem(String name, String groupName) throws JavaGraphicsException {
            for (Resource resource : ResourceGroupManager.getInstance().getResourceGroup(groupName).getResources()) {
                if (resource.isUnloaded()) {
                    resource.setName(name);
                    return resource;
                }
            }

            return manager.create(name, groupName);
        }
    }
}
