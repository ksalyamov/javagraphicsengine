package com.jge.core.beans;

import com.jge.core.exceptions.JavaGraphicsException;
import com.jge.core.managers.SceneManager;
import com.jge.core.maths.Vector2d;

/**
 * Класс камеры
 * @author Salyamov Kirill
 *
 */
public class Camera {
    public static enum Axis {
        X, Y;
    }

    private Vector2d position;
    private float zoom;
    private float angle;
    private SceneManager sceneManager;

    public Camera(SceneManager sceneManager) {
        super();
        this.position = new Vector2d(0.0f, 0.0f);
        this.zoom = 0.0f;
        this.angle = 0.0f;
        this.sceneManager = sceneManager;
    }

    public void rotate(float angle) {
        this.angle = angle;
    }

    public void zoom(float zoom) {
        this.zoom += zoom;
    }

    public void zoomIn(float zoom) {
        this.zoom += Math.abs(zoom);
    }

    public void zoomOut(float zoom) {
        this.zoom -= Math.abs(zoom);
    }

    /**
     * Метод двигает камеру на diff по оси axis 
     * @param axis Ось, по которой необходимо произвести смещенеие
     * @param diff Смещение
     */
    public void move(Camera.Axis axis, float diff) {
        if (axis == Axis.X)
            position.setX(position.getX() + diff);
        else if (axis == Axis.Y)
            position.setY(position.getY() + diff);
    }

    public void setPosition(float x, float y) {
        position.setX(x);
        position.setY(y);
    }

    public float getX() {
        return this.position.getX();
    }

    public float getY() {
        return this.position.getY();
    }

    public float getZoom() {
        return this.zoom;
    }

    public float getAngle() {
        return this.angle;
    }
    
    public void renderScene(Viewport viewport) throws JavaGraphicsException {
        sceneManager.renderScene(this, viewport);
    }
}
