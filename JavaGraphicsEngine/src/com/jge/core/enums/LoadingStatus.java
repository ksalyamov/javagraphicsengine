package com.jge.core.enums;

public enum LoadingStatus {
    NONE,       // Не загружен
    UNLOADED,	// В процессе загрузки
    LOADED,     // Полностью загружен
    UNLOADING,	// Отгружается
    PREPARED,	// Полностью готов к загрузке
    PREPARING	// Подготавливается к загрузке
}
