package com.jge.core.managers;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import com.jge.core.Root;
import com.jge.core.beans.Camera;
import com.jge.core.beans.SceneNode;
import com.jge.core.beans.SimpleRenderable;
import com.jge.core.beans.Viewport;
import com.jge.core.enums.LoadingStatus;
import com.jge.core.exceptions.JavaGraphicsException;
import com.jge.core.interfaces.RenderSystem;
import com.jge.core.interfaces.RenderWindowListener;
import com.jge.core.interfaces.ThreadedLoadingQueue;
import com.jge.core.objects.Sprite;
import com.jge.core.objects.SubSprite;

public abstract class SceneManager {
    private Map<String, Sprite> createdSprites;
    private Map<String, SubSprite> createBaseSprites;
    private SceneNode rootSceneNode;
    private long lastElapsedTime;
    private Camera camera;

    protected static SceneManager singleton;
    public static SceneManager getInstance() {
        return singleton;
    }

    protected SceneManager(RenderSystem renderSystem) {
        super();
        this.rootSceneNode = new SceneNode();

        this.createdSprites = new HashMap<>();
        this.createBaseSprites = new HashMap<>();
    }


    public SceneNode getRootSceneNode() {
        return rootSceneNode;
    }

    public Sprite createSprite(String name, String baseSprite) throws JavaGraphicsException {
        Sprite sprite = getSprite(name);
        if (sprite != null) {
            throw new JavaGraphicsException(String.format("Sprite with name \"%s\" already exists!", name));
        }
        sprite = new Sprite(name, getBaseSprite(baseSprite));
        setSprite(name, sprite);
        return sprite;
    }

    public Sprite getSprite(String name) {
        return this.createdSprites.get(name);
    }
    
    protected void setSprite(String name, Sprite sprite) {
        createdSprites.put(name, sprite);
    }

    public void displayScene() throws JavaGraphicsException {
        RenderWindowListener listener = Root.getInstance().getRenderSystem().getCurrentRenderWindow().getRenderWindowListener();
        long currElapsedTime = 0;

        //if (lastElapsedTime > 0)
        currElapsedTime = (new Date().getTime());

        float deltaTime = (currElapsedTime - lastElapsedTime)/1000.0f;
        deltaTime = deltaTime >= 1.0f ? 1.0f : deltaTime;

        if (listener != null) {
            try {
                listener.updateScene(deltaTime);
            } catch (JavaGraphicsException e) {
                throw e;
            }
        }

        try {
            ResourceGroupManager.getInstance().getLoadingQueue().loading();
        } catch (JavaGraphicsException e) {
            throw e;
        }

        try {
            RenderSystem.getInstance().beginFrame();
            RenderSystem.getInstance().render();
            RenderSystem.getInstance().endFrame();
        } catch (JavaGraphicsException e) {
            throw e;
        }

        lastElapsedTime = currElapsedTime;
    }

    public void initializeScene() throws JavaGraphicsException {
        RenderWindowListener listener = Root.getInstance().getRenderSystem().getCurrentRenderWindow().getRenderWindowListener();
        if (listener != null) {
            try {
                listener.createScene();
            } catch (JavaGraphicsException e) {
                throw e;
            }
        }
    }

    public void destroyScene() throws JavaGraphicsException {
        ResourceGroupManager.getInstance().unloadAllResourceGroups();
    }

    public SubSprite getBaseSprite(String name) throws JavaGraphicsException {
        return this.createBaseSprites.get(name);
    }

    public void registerBaseSprite(String name, int width, int height) throws JavaGraphicsException {
        if (getBaseSprite(name) != null) {
            throw new JavaGraphicsException(String.format("Base sprite with name \"%s\" already exists!", name));
        }
        SubSprite subSprite = new SubSprite(name, width, height);
        this.createBaseSprites.put(name, subSprite);
    }

    public Camera createOrRetriveCamera() {
        if (this.camera == null) {
            this.camera = new Camera(this);
        }
        return this.camera;
    }

    public boolean isCameraCreated() {
        return this.camera != null;
    }

    public void renderScene(Camera camera, Viewport viewport) throws JavaGraphicsException {        
        RenderSystem rs = RenderSystem.getInstance();
        
        if (rs != null) {
            // FIXME: begin not here!
            rs.beginFrame();
            rs.render();
            rs.endFrame();
        }
    }

}
