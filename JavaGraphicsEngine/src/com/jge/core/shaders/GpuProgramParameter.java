package com.jge.core.shaders;

public class GpuProgramParameter {
    public static enum Type {
        INTEGER,
        FLOAT,
        DOUBLE,
        TEXTURE
    }

    private GpuProgramParameter.Type type;
    private Object object;
    private String name;

    public GpuProgramParameter() {
        super();
    }
    public GpuProgramParameter(String name, GpuProgramParameter.Type type, Object object) {
        this();
        this.type = type;
        this.object = object;
        this.name = name;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public  GpuProgramParameter.Type getType() {
        return type;
    }
    public void setType(GpuProgramParameter.Type type) {
        this.type = type;
    }
    public Object getObject() {
        return object;
    }
    public void setObject(Object object) {
        this.object = object;
    }
}
