package com.jge.jogl.core.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.IntBuffer;
import java.util.HashMap;
import java.util.Map;

import javax.media.opengl.GL2;

import com.jge.core.beans.ResourceGroup;
import com.jge.core.enums.ExceptionType;
import com.jge.core.exceptions.JavaGraphicsException;
import com.jge.core.interfaces.GpuProgram;
import com.jge.core.managers.ResourceGroupManager;
import com.jge.core.managers.ResourceManager;
import com.jge.core.shaders.GpuProgramParameter;
import com.jogamp.common.nio.Buffers;

public class GpuProgramOpenGL extends GpuProgram {
    private int idCodeShader;
    private int idShaderProgram;
    private Map<String, Integer> parameters;

    public GpuProgramOpenGL(ResourceManager creator, String name,
            String groupName, Type type) {
        super(creator, name, groupName, type);
    }

    @Override
    public void preparing() throws JavaGraphicsException {
        if (getSource() == null || "".equals(getSource().trim())) {
            // загружаем из файла
            if (getSourceFile() == null || "".equals(getSourceFile().trim())) {
                throw new JavaGraphicsException("Cannot load shader - there is no file");
            }

            FileInputStream fis = null;
            try {
                ResourceGroup group = ResourceGroupManager.getInstance().getResourceGroup(getGroupName());
                File file = group.getFile(getSourceFile());
                if (file == null) {
                    throw new JavaGraphicsException(String.format("Cannot load resource by name \"%s\"", getSourceFile()));
                }

                fis = new FileInputStream(file);
                setSource(GpuProgram.readFromStream(fis));
            } catch (FileNotFoundException e) {
                throw new JavaGraphicsException(ExceptionType.FILE_NOT_FOUND, String.format("Cannot load file %s",  getSourceFile()), e);
            } catch (IOException e) {
                throw new JavaGraphicsException(ExceptionType.DEFAULT, String.format("Cannot load file %s",  getSourceFile()), e);
            } finally {
                if (fis != null) {
                    try {
                        fis.close();
                    } catch (IOException e) {
                        throw new JavaGraphicsException(e);
                    }
                }
            }

            if (getSource() == null || "".equals(getSource().trim())) {
                throw new JavaGraphicsException(ExceptionType.BAD_FILE_FORMAT, String.format("Cannot load file %s",  getSourceFile()));
            }

        }
    }

    @Override
    public void loading() throws JavaGraphicsException {
        RenderSystemOpenGL rs = ((RenderSystemOpenGL) RenderSystemOpenGL.getInstance());
        GL2 gl = rs.getGL2();

        if (getType() == Type.GPT_VERTEX_SHADER) {
            idCodeShader = gl.glCreateShader(GL2.GL_VERTEX_SHADER);
        } else {
            idCodeShader = gl.glCreateShader(GL2.GL_FRAGMENT_SHADER);
        }

        gl.glShaderSource(idCodeShader, 1, new String[]{getSource()}, null, 0);
        gl.glCompileShader(idCodeShader);

        idShaderProgram = gl.glCreateProgram();

        gl.glAttachShader(idShaderProgram, idCodeShader);
        gl.glLinkProgram(idShaderProgram);
        gl.glValidateProgram(idShaderProgram);

        System.out.printf("Shader \"%s\" was loaded\n", getName());

        IntBuffer params = Buffers.newDirectIntBuffer(1);
        gl.glGetObjectParameterivARB(idShaderProgram, GL2.GL_OBJECT_ACTIVE_UNIFORMS_ARB, params);
        int numParams = params.get(0);

        if (numParams > 0) {
            parameters = new HashMap<String, Integer>();
            for (int i=0; i<numParams; i++) {
                int[] size = new int [12];
                int[] type = new int [12];
                byte[] name = new byte[256];
                gl.glGetActiveUniformARB(idShaderProgram, i, 256, null, 0, size, 0, type, 0, name, 0);
                String paramName;
                try {
                    paramName = new String(name, "UTF-8").trim();
                    parameters.put(paramName, gl.glGetUniformLocation(idShaderProgram, paramName));
                } catch (UnsupportedEncodingException e) {
                    // TODO something!
                    e.printStackTrace();
                }
            }
        }		
    }

    @Override
    public void loadImpl() throws JavaGraphicsException {
        preparing();
        loading();
    }

    public void unloadImpl() throws JavaGraphicsException {
        RenderSystemOpenGL rs = ((RenderSystemOpenGL) RenderSystemOpenGL.getInstance());
        GL2 gl = rs.getGL2();

        gl.glDeleteShader(idCodeShader);
        gl.glDeleteProgram(idShaderProgram);
    }

    @Override
    public void bindProgram() throws JavaGraphicsException {
        RenderSystemOpenGL rs = ((RenderSystemOpenGL) RenderSystemOpenGL.getInstance());
        GL2 gl = rs.getGL2();

        gl.glUseProgram(idShaderProgram);
    }

    @Override
    public void unbindProgram() throws JavaGraphicsException {
        RenderSystemOpenGL rs = ((RenderSystemOpenGL) RenderSystemOpenGL.getInstance());
        rs.getGL2().glUseProgram(0);
    }

    public int getIdCodeShader() {
        return idCodeShader;
    }

    public int getIdShaderProgram() {
        return idShaderProgram;
    }

    @Override
    protected void prepareImpl() throws JavaGraphicsException {
        preparing();
    }

    @Override
    protected void bindProgramParameterImpl(GpuProgramParameter parameter)
            throws JavaGraphicsException {
        if (parameters != null && parameters.get(parameter.getName()) != null) {
            int paramId = parameters.get(parameter.getName()).intValue();
            if (paramId >= 0) {
                RenderSystemOpenGL rs = ((RenderSystemOpenGL) RenderSystemOpenGL.getInstance());
                GL2 gl = rs.getGL2();

                if (parameter.getType() == GpuProgramParameter.Type.FLOAT) {
                    gl.glUniform1f(paramId, ((Float)parameter.getObject()).floatValue());
                } else if (parameter.getType() == GpuProgramParameter.Type.INTEGER) {
                    gl.glUniform1i(paramId, ((Integer)parameter.getObject()).intValue());
                } else if (parameter.getType() == GpuProgramParameter.Type.DOUBLE) {
                    gl.glUniform1f(paramId, ((Double)parameter.getObject()).floatValue());
                } else if (parameter.getType() == GpuProgramParameter.Type.TEXTURE) {
                    throw new JavaGraphicsException("Method bindProgramParameter with Texture is not implemented");
                } 				
            }
        }
    }

}
