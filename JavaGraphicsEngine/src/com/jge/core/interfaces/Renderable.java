package com.jge.core.interfaces;

import java.util.List;

import com.jge.core.beans.RenderOperation;
import com.jge.core.beans.SceneNode;
import com.jge.core.exceptions.JavaGraphicsException;
import com.jge.core.maths.Matrix;
import com.jge.core.shaders.Pass;
import com.jge.core.shaders.Technique;


public interface Renderable {	
    Matrix getWorldTransform() throws JavaGraphicsException;
    void preRender(RenderSystem renderSystem, Pass pass) throws JavaGraphicsException;
    void postRender(RenderSystem renderSystem, Pass pass) throws JavaGraphicsException;
    RenderOperation getRenderOperation() throws JavaGraphicsException;
    List<Technique> getTechniques() throws JavaGraphicsException;
    Technique getTechnique() throws JavaGraphicsException;
}
