package com.jge.core;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.jge.core.beans.GroupLocation;
import com.jge.core.beans.ResourceGroup;
import com.jge.core.enums.ExceptionType;
import com.jge.core.enums.ResourceGroupName;
import com.jge.core.exceptions.JavaGraphicsException;
import com.jge.core.interfaces.RenderSystem;
import com.jge.core.interfaces.RenderWindow;
import com.jge.core.interfaces.RenderedWindow;
import com.jge.core.managers.GpuProgramManager;
import com.jge.core.managers.MaterialManager;
import com.jge.core.managers.ResourceGroupManager;
import com.jge.core.managers.ResourceManager;
import com.jge.core.managers.SceneManager;
import com.jge.core.managers.TextureManager;

public class Root {
    private static Root root;
    private RenderSystem renderSystem;

    public Root() {
        super();
        Root.root = this;
    }

    public void setRenderSystem(RenderSystem renderSystem) {
        this.renderSystem = renderSystem;
    }

    public ResourceGroupManager getResourceGroupManager() {
        return renderSystem.getResourceGroupManager();
    }
    public static Root getInstance() {
        return Root.root;
    }
    public SceneManager getSceneManager() {
        return renderSystem.getSceneManager();
    }
    public TextureManager getTextureManager() {
        return renderSystem.getTextureManager();
    }
    public RenderSystem getRenderSystem() {
        return renderSystem;
    }
    public RenderWindow getRenderedWindow() {
        return renderSystem.getRenderWindow();
    }

    public MaterialManager getMaterialManager() {
        return renderSystem.getMaterialManager();
    }

    public GpuProgramManager getGpuProgramManager() {
        return renderSystem.getGpuProgramManager();
    }


    public static void loadResourcesFromFile(File file) throws JavaGraphicsException {
        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;
        try {
            builder = builderFactory.newDocumentBuilder();

            Document document = builder.parse(new FileInputStream(file));
            XPath xPath =  XPathFactory.newInstance().newXPath();

            NodeList nodeList = null;
            nodeList = (NodeList) xPath.compile("/resources/group").evaluate(document, XPathConstants.NODESET);
            for (int i = 0; nodeList != null && i < nodeList.getLength(); i++) {
                Node resourceGroupItem = nodeList.item(i);
                String resGroupName = resourceGroupItem.getAttributes().getNamedItem("name").getNodeValue();

                //System.out.println(resGroupName);

                String path = String.format("/resources/group[%d]/location", i+1);
                NodeList nodeListLocations =(NodeList) xPath.compile(path).evaluate(document, XPathConstants.NODESET);


                ResourceGroup group = null;
                group = ResourceGroupManager.getInstance().getResourceGroup(resGroupName);
                for (int j = 0; nodeListLocations != null && j < nodeListLocations.getLength(); j++) {
                    Node resourceLocation = nodeListLocations.item(j);

                    String subPath = String.format("/resources/group[%d]/location[%d]/format", i+1, j+1);
                    String resFormat = (String) xPath.compile(subPath).evaluate(document, XPathConstants.STRING);

                    subPath = String.format("/resources/group[%d]/location[%d]/path", i+1, j+1);
                    String resPath = (String) xPath.compile(subPath).evaluate(document, XPathConstants.STRING);

                    //System.out.println("\t" + resFormat);
                    //System.out.println("\t" + resPath);

                    group.getLocations().add(new GroupLocation(new File(resPath), resFormat));
                }

                if (!resGroupName.trim().equals(ResourceGroupName.SHADERS.getName())) {
                    for (GroupLocation location : group.getLocations()) {
                        for (File f : location.getFiles()) {
                            ResourceGroupManager.getInstance().createResource(group.getName(), f.getName());
                        }
                    }
                    ResourceGroupManager.getInstance().prepareResourceGroup(resGroupName);
                }
            }
            ResourceGroupManager.getInstance().loadAllResourceGroups();
        } catch (ParserConfigurationException e) {
            throw new JavaGraphicsException(ExceptionType.DEFAULT, e);
        } catch (FileNotFoundException e) {
            throw new JavaGraphicsException(ExceptionType.FILE_NOT_FOUND, e);
        } catch (SAXException e) {
            throw new JavaGraphicsException(ExceptionType.DEFAULT, e);
        } catch (IOException e) {
            throw new JavaGraphicsException(ExceptionType.DEFAULT, e);
        } catch (XPathExpressionException e) {
            throw new JavaGraphicsException(ExceptionType.DEFAULT, e);
        }
    }

    /*public static void main(String... args) {
		try {
			loadResourcesFromFile(new File("..\\JavaGraphicsEngine_JOGL\\media\\resources.xml"));
		} catch (JavaGraphicsException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}*/
}


