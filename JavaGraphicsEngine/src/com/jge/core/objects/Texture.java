package com.jge.core.objects;

import com.jge.core.beans.Resource;
import com.jge.core.enums.LoadingStatus;
import com.jge.core.exceptions.JavaGraphicsException;
import com.jge.core.interfaces.StatedItem;
import com.jge.core.interfaces.TaskItem;
import com.jge.core.managers.ResourceManager;
import com.jge.core.managers.SceneManager;
import com.jge.core.managers.TextureManager;

public abstract class Texture extends Resource {

    protected Texture(ResourceManager creator, String name, String groupName) {
        super(creator, name, groupName);
    }
}
