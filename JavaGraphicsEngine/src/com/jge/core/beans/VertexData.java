package com.jge.core.beans;

public class VertexData {
    private VertexBufferBinding vertexBufferBinding;
    private VertexDeclaration vertexDeclaration;
    private int vertexCount;

    public VertexBufferBinding getVertexBufferBinding() {
        return vertexBufferBinding;
    }
    public void setVertexBufferBinding(VertexBufferBinding vertexBufferBinding) {
        this.vertexBufferBinding = vertexBufferBinding;
    }
    public VertexDeclaration getVertexDeclaration() {
        return vertexDeclaration;
    }
    public void setVertexDeclaration(VertexDeclaration vertexDeclaration) {
        this.vertexDeclaration = vertexDeclaration;
    }
    public int getVertexCount() {
        return vertexCount;
    }
    public void setVertexCount(int vertexCount) {
        this.vertexCount = vertexCount;
    }
}
