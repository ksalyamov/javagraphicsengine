package com.jge.core.enums;

public enum ResourceGroupName {
    TEXTURES("Textures"),
    MATERIALS("Materials"), 
    SHADERS("Shaders");

    private String name;

    ResourceGroupName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

}
