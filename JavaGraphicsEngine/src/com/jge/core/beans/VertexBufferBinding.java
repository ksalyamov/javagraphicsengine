package com.jge.core.beans;

import java.util.HashMap;
import java.util.Map;

import com.jge.core.exceptions.JavaGraphicsException;
import com.jge.core.interfaces.HardwareVertexBuffer;

public class VertexBufferBinding {
    private Map<Short, HardwareVertexBuffer> bindingMap;

    public VertexBufferBinding() {
        super();
        this.bindingMap = new HashMap<Short, HardwareVertexBuffer>();
    }

    public void setBinding(short source, HardwareVertexBuffer buffer) throws JavaGraphicsException {
        bindingMap.put(source, buffer);
    }

    public void unsetBinding(short source) throws JavaGraphicsException  {
        if (isBufferBound(source)) {
            // TODO Такого item'а не найдено
        }
        bindingMap.remove(source);
    }

    public boolean isBufferBound(short source)  {
        return bindingMap.containsKey(source);
    }

    public HardwareVertexBuffer getBuffer(short source) {
        if (isBufferBound(source)) {
            // TODO Такого item'а не найдено
        }
        return bindingMap.get(source);
    }
}
