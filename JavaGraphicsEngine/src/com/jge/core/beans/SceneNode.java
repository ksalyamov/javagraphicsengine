package com.jge.core.beans;

import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import com.jge.core.enums.ExceptionType;
import com.jge.core.exceptions.JavaGraphicsException;
import com.jge.core.interfaces.Moveable;
import com.jge.core.interfaces.Naming;
import com.jge.core.interfaces.RenderQueue;
import com.jge.core.managers.SceneManager;
import com.jge.core.maths.Matrix;
import com.jge.core.maths.Vector2d;


public class SceneNode implements Naming{
    private static final Map<String, SceneNode> allSceneNodes = new HashMap<>();
    private static final String DEFAULT_NAME = String.format("___%s_%d", SceneNode.class.toString(), allSceneNodes.hashCode()); 

    private String name;
    private SceneNode prevNode;
    private List<SceneNode> nextNodes;
    private Map<String, MoveableObject> moveableObjects;

    private boolean visible;
    private boolean independent;
    private double angle;
    private Vector2d position;

    public SceneNode() {
        super();
        this.moveableObjects = new HashMap<>();
        this.visible = true;
        this.angle = 0;
        this.position = new Vector2d(0, 0);
        this.independent = false;
    }

    public void attachObject(MoveableObject mo) throws JavaGraphicsException {
        if (moveableObjects.get(mo.getName()) != null) {
            // TODO logger
        }
        moveableObjects.put(mo.getName(), mo);
        mo.setSceneNode(this);
    }

    public SceneNode createChildNode() {
        SceneNode nextNode = new SceneNode();

        if (nextNodes == null) {
            nextNodes = new ArrayList<>();
        }
        nextNodes.add(nextNode);
        nextNode.prevNode = this;

        return nextNode;
    }

    public void findVisibleObjects(Camera camera, RenderQueue queue, boolean includeChildren) throws JavaGraphicsException {
        for (Map.Entry<String, MoveableObject> entry : moveableObjects.entrySet()) {

            if (!isVisible()) continue;

            // TODO проверка что объект мы видим
            queue.processVisibleObject(camera, entry.getValue());

            if (includeChildren && nextNodes != null) {
                for (SceneNode node : nextNodes) {
                    node.findVisibleObjects(camera, queue, includeChildren);
                }
            }
        }
    }

    public SceneNode getParentNode() {
        return prevNode;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public boolean isIndependent() {
        return independent;
    }

    public void setIndependent(boolean independent) {
        this.independent = independent;
    }

    public double getAngle() {
        return angle;
    }

    public void setAngle(double angle) {
        this.angle = angle;
    }

    @Override
    public void setName(String name) throws JavaGraphicsException {
        this.name = name;
    }

    @Override
    public String getName() throws JavaGraphicsException {
        return name;
    }

    public Vector2d getPosition() {
        return position;
    }

    public void setPosition(Vector2d position) {
        this.position = position;
    }

    public void setPosition(float x, float y) {
        this.position.setX(x);
        this.position.setY(y);
    }

    public Matrix getNodeTransform() {
        Matrix matrix = new Matrix();

        Matrix matPos = new Matrix();
        matPos.identity();

        Matrix matRot = new Matrix();
        matRot.rotateIn2dWorld((float) getAngle());

        matrix.setMatrix(matPos);
        matrix.setMatrix(matPos.multiply(matRot));

        matrix.setPosition(position);

        return matrix;
    }
}
