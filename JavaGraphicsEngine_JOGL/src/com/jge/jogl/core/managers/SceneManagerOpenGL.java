package com.jge.jogl.core.managers;

import com.jge.core.interfaces.RenderSystem;
import com.jge.core.managers.SceneManager;

public class SceneManagerOpenGL extends SceneManager {

    public SceneManagerOpenGL(RenderSystem renderSystem) {
        super(renderSystem);
        SceneManager.singleton = this;
    }
}
