package com.jge.jogl.beans;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;

import com.jge.core.beans.RenderOperation;
import com.jge.core.beans.VertexElement;
import com.jge.core.interfaces.HardwareBuffer;

public class OpenGLMappings {
    public static int map(HardwareBuffer.Usage usage) {
        switch(usage) {
        case HBU_STATIC:
        case HBU_STATIC_WRITE_ONLY:
            return GL2.GL_STATIC_DRAW;
        case HBU_DYNAMIC:
        case HBU_DYNAMIC_WRITE_ONLY:
            return GL.GL_DYNAMIC_DRAW;
            /*case HBU_DYNAMIC_WRITE_ONLY_DISCARDABLE:
		            return  GL_STREAM_DRAW_ARB;*/
        default:
            return GL.GL_DYNAMIC_DRAW;
        }
    }

    public static int map(VertexElement.Type type) {
        switch (type) {
        case VET_FLOAT:
        case VET_FLOAT1:
        case VET_FLOAT2:
        case VET_FLOAT3:
        case VET_FLOAT4:
            return GL.GL_FLOAT;
        case VET_SHORT:
        case VET_SHORT1:
        case VET_SHORT2:
        case VET_SHORT3:
        case VET_SHORT4:
            return GL.GL_SHORT;
        case VET_COLOUR:
        case VET_COLOUR_ABGR:
        case VET_COLOUR_ARGB:
            return GL.GL_FLOAT;
        case VET_UBYTE4:
            return GL.GL_UNSIGNED_BYTE;

        default:
            return 0;
        }
    }

    public static int map(RenderOperation.Type type) {
        switch (type) {
        case ROT_POINT_LIST:
            return GL.GL_POINTS;
        case ROT_LINE_LIST:
            return GL.GL_LINES;
        case ROT_LINE_STRIP:
            return GL.GL_LINE_STRIP;
        case ROT_TRIANGLE_LIST:
            return GL.GL_TRIANGLES;
        case ROT_TRIANGLE_STRIP:
            return GL.GL_TRIANGLE_STRIP;
        case ROT_TRIANGLE_FAN:
            return GL.GL_TRIANGLE_FAN;
        }
        return -1;
    }
}
