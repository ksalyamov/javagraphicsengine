package com.jge.core.interfaces;

import com.jge.core.exceptions.JavaGraphicsException;

/**
 * 
 * @author Салямов Кирилл
 *
 */
public interface TaskItem extends StatedItem {
    /**
     * Метод, выполняющий загрузку данных в отдельном потоке. 
     * @throws JavaGraphicsException  Внутренняя ошибка
     */
    public void preparing() throws JavaGraphicsException;
    /**
     * Метод, выполнящий загрузку данных в необходимый контекст. Выполняется в основном потоке
     * @throws JavaGraphicsException  Внутренняя ошибка
     */
    public void loading() throws JavaGraphicsException;
    /**
     * Метод возвращает краткую информацию о созданном объекте
     * @return Информация о созданном объекте
     * @throws JavaGraphicsException Внутренняя ошибка
     */
    public String info() throws JavaGraphicsException;
}
