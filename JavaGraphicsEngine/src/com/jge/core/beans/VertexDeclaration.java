package com.jge.core.beans;

import java.util.ArrayList;
import java.util.List;

public class VertexDeclaration {
    private List<VertexElement> elementList;

    public VertexDeclaration() {
        super();
        this.elementList = new ArrayList<VertexElement>();
    }

    public void addVertex(short source, long offset, VertexElement.Type type, VertexElement.Semantic semantic) {
        elementList.add(new VertexElement(source, offset, type, semantic));
    }

    public void removeElement(int index) {
        elementList.remove(index);
    }

    public void removeAllElements() {
        this.elementList.clear();
    }

    public int getVertexSize(short source) {
        int size = 0;
        for (VertexElement element : this.elementList) {
            if (element.getSource() != source) { 
                continue;
            }
            size += element.getSize();
        }
        return size;
    }

    public List<VertexElement> getElementList() {
        return this.elementList;
    }

}
