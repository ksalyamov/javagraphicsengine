package com.jge.core.maths;

public class Vector2d {
    private float x;
    private float y;

    public Vector2d() {
        super();
    }

    public Vector2d(float x, float y) {
        this();
        this.x = x;
        this.y = y;
    }

    public Vector2d(float scale) {
        this(scale, scale);
    }

    public Vector2d normalize() {
        float length = length();
        x = x/length;
        y = y/length;
        return this;
    }

    public float length() {
        float sqrt = (float) Math.sqrt(x*x + y*y);
        return sqrt;
    }

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }
}
