package com.jge.core.managers;

import java.util.HashMap;
import java.util.Map;

import com.jge.core.beans.Resource;
import com.jge.core.enums.ResourceGroupName;
import com.jge.core.exceptions.JavaGraphicsException;
import com.jge.core.objects.Texture;

public abstract class TextureManager extends ResourceManager{
    protected static TextureManager instance;

    public static TextureManager getInstance() {
        return instance;
    }

    protected TextureManager() throws JavaGraphicsException {
        super(ResourceGroupName.TEXTURES.getName());
        TextureManager.instance = this;
    }
}
