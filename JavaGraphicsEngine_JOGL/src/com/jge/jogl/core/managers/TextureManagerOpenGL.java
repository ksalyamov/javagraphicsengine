package com.jge.jogl.core.managers;

import com.jge.core.beans.Resource;
import com.jge.core.enums.ExceptionType;
import com.jge.core.exceptions.JavaGraphicsException;
import com.jge.core.interfaces.RenderSystem;
import com.jge.core.managers.ResourceManager;
import com.jge.core.managers.SceneManager;
import com.jge.core.managers.TextureManager;
import com.jge.core.objects.Texture;
import com.jge.jogl.core.impl.RenderSystemOpenGL;
import com.jge.jogl.core.objects.TextureOpenGL;

public class TextureManagerOpenGL extends TextureManager {	

    public TextureManagerOpenGL()
            throws JavaGraphicsException {
        super();
    }

    @Override
    protected Resource createImpl(ResourceManager creator, String name,
            String group) throws JavaGraphicsException {
        return new TextureOpenGL(creator, name, group);
    }



}
