package com.jge.core.beans;

import java.util.List;

import com.jge.core.exceptions.JavaGraphicsException;
import com.jge.core.interfaces.RenderQueue;
import com.jge.core.interfaces.RenderSystem;
import com.jge.core.interfaces.Renderable;
import com.jge.core.maths.Matrix;
import com.jge.core.shaders.Material;
import com.jge.core.shaders.Pass;
import com.jge.core.shaders.Technique;

public abstract class SimpleRenderable extends MoveableObject implements Renderable {
    protected Material material;

    @Override
    public void updateRenderQueue(RenderQueue queue) throws JavaGraphicsException {
        queue.addRenderable(this);
    }

    @Override
    public Matrix getWorldTransform() throws JavaGraphicsException {
        return getSceneNode().getNodeTransform();
    }

    public Material getMaterial() {
        return material;
    }

    public void setMaterial(Material material) {
        this.material = material;
    }

    public List<Technique> getTechniques() throws JavaGraphicsException {
        if (material != null) {
            return material.getTechniques();
        }
        return null;
    }

    public Technique getTechnique() throws JavaGraphicsException {
        return material != null ? material.getTechnique(0) : null;
    }

    @Override
    public void preRender(RenderSystem renderSystem, Pass pass)
            throws JavaGraphicsException {
        RenderSystem rs = RenderSystem.getInstance();

        if (pass != null) {
            if (pass.getVertexProgramUsage() != null && pass.getVertexProgramUsage().getGpuProgram() != null) {
                //rs.bindGpuProgram(pass.getVertexProgramUsage().getGpuProgram());
                pass.getVertexProgramUsage().getGpuProgram().bindProgram();
            }

            if (pass.getPixelProgramUsage() != null && pass.getPixelProgramUsage().getGpuProgram() != null) {
                //rs.bindGpuProgram(pass.getPixelProgramUsage().getGpuProgram());
                pass.getPixelProgramUsage().getGpuProgram().bindProgram();
            }


        }
    }

    @Override
    public void postRender(RenderSystem renderSystem, Pass pass)
            throws JavaGraphicsException {
        RenderSystem rs = RenderSystem.getInstance();
        if (pass != null) {
            if (pass != null) {
                if (pass.getPixelProgramUsage() != null && pass.getPixelProgramUsage().getGpuProgram() != null) {
                    //rs.unbindGpuProgram(pass.getPixelProgramUsage().getGpuProgram());
                    pass.getPixelProgramUsage().getGpuProgram().unbindProgram();
                }

                if (pass.getVertexProgramUsage() != null && pass.getVertexProgramUsage().getGpuProgram() != null) {
                    //rs.unbindGpuProgram(pass.getVertexProgramUsage().getGpuProgram());
                    pass.getVertexProgramUsage().getGpuProgram().unbindProgram();
                }
            }
        }
    }
}
