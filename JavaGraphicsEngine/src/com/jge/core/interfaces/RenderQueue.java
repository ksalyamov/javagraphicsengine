package com.jge.core.interfaces;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jge.core.beans.Camera;
import com.jge.core.beans.GpuProgramUsage;
import com.jge.core.exceptions.JavaGraphicsException;
import com.jge.core.shaders.GpuProgramParameter;
import com.jge.core.shaders.Pass;
import com.jge.core.shaders.Technique;

public class RenderQueue {
    private List<Renderable> renderableObjects;
    private Map<Renderable, List<Pass>> passesOfObjects;

    public RenderQueue() {
        super();
        renderableObjects = new ArrayList<Renderable>();
        this.passesOfObjects = new HashMap<Renderable, List<Pass>>();
    }

    public void addRenderable(Renderable renderable) throws JavaGraphicsException {
        if (renderableObjects.contains(renderable)) {
            // TODO throws
        }
        renderableObjects.add(renderable);

        /*Technique technique = renderable.getTechnique();
		if (technique != null) {
			passesOfObjects.put(renderable, technique.getPasses());
		}*/
    }

    public void clearQueue() {
        renderableObjects.clear();
    }


    public void processVisibleObject(Camera camera, Moveable moveable) throws JavaGraphicsException {
        if (moveable.isVisible()) {
            moveable.updateRenderQueue(this);
        }
    }

    public void fireRenderQueue(RenderSystem renderSystem) throws JavaGraphicsException {
        for (Renderable r : renderableObjects) {


            Technique tech = r.getTechnique();

            if (tech != null) {
                for (Pass pass : tech.getPasses()) {
                    renderSingleObject(r, renderSystem, pass);
                }
            } else {
                renderSingleObject(r, renderSystem, null);
            }
        }
    }

    private void renderSingleObject(Renderable r, RenderSystem rs, Pass pass) throws JavaGraphicsException {
        r.preRender(rs, pass);
        rs.setWorldTransform(r.getWorldTransform());
        Collection<GpuProgramParameter> parameters = pass.getParameters();
        if (parameters != null) {
            for (GpuProgramParameter parameter : pass.getParameters()) {
                bindGpuProgramParameters(pass.getPixelProgramUsage(), parameter);
                bindGpuProgramParameters(pass.getVertexProgramUsage(), parameter);
            }
        }
        rs.render(r.getRenderOperation());
        r.postRender(rs, pass);
    }

    public boolean isEmptyQueue() {
        return renderableObjects.isEmpty();
    }

    private void bindGpuProgramParameters(GpuProgramUsage gpUsage, GpuProgramParameter parameter) throws JavaGraphicsException {
        if (gpUsage != null) {
            GpuProgram program = gpUsage.getGpuProgram();
            if (program != null) {
                program.bindProgramParameter(parameter);
            }
        }
    }

}
