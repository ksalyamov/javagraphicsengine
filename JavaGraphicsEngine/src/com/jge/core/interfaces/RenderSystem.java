package com.jge.core.interfaces;

import com.jge.core.beans.RenderOperation;
import com.jge.core.beans.SceneNode;
import com.jge.core.enums.ExceptionType;
import com.jge.core.enums.ResourceGroupName;
import com.jge.core.exceptions.JavaGraphicsException;
import com.jge.core.managers.GpuProgramManager;
import com.jge.core.managers.HardwareBufferManager;
import com.jge.core.managers.MaterialManager;
import com.jge.core.managers.ResourceGroupManager;
import com.jge.core.managers.SceneManager;
import com.jge.core.managers.TextureManager;
import com.jge.core.maths.Matrix;
import com.jge.core.maths.Vector2d;
import com.jge.core.objects.Texture;

public abstract class RenderSystem {
    private GpuProgramManager gpuProgramManager;
    private HardwareBufferManager hardwareBufferManager;
    private SceneManager sceneManager;
    private TextureManager textureManager;
    private ResourceGroupManager resourceGroupManager;
    private MaterialManager materialManager;

    protected static RenderSystem singleton;
    private RenderQueue renderQueue;
    protected RenderWindow renderWindow;
    
    protected float background[] = {0, 0, 0};

    protected RenderSystem() {
        super();
        this.renderQueue = new RenderQueue();
    }

    public static RenderSystem getInstance() {
        return RenderSystem.singleton;
    }

    public RenderWindow getCurrentRenderWindow() {
        return renderWindow;
    }

    public RenderQueue getRenderQueue() {
        return this.renderQueue;
    }

    public void setPosition(float x, float y) {
        setPosition(new Vector2d(x, y));
    }

    public void render(RenderOperation ro) throws JavaGraphicsException {
        renderImpl(ro);
    }

    public void beginFrame() throws JavaGraphicsException {
        this.clearScene();
        SceneManager.getInstance().getRootSceneNode().findVisibleObjects(null, getRenderQueue(), true);
    }

    public void endFrame() throws JavaGraphicsException {
        // Do nothing
    }

    public void render() throws JavaGraphicsException {
        if (!getRenderQueue().isEmptyQueue()) {
            getRenderQueue().fireRenderQueue(this);
            getRenderQueue().clearQueue();
        }
    }

    public void initialize(String name, int width, int height) throws JavaGraphicsException {
        renderWindow = createRenderWindow(name, width, height);
        if (renderWindow == null) {
            // TODO exception
        }
        initializeImpl();
    }

    protected void setGpuProgramManager(GpuProgramManager gpuProgramManager) {
        this.gpuProgramManager = gpuProgramManager;
    }

    protected void setHardwareBufferManager(
            HardwareBufferManager hardwareBufferManager) {
        this.hardwareBufferManager = hardwareBufferManager;
    }

    protected void setSceneManager(SceneManager sceneManager) {
        this.sceneManager = sceneManager;
    }

    protected void setTextureManager(TextureManager textureManager) {
        this.textureManager = textureManager;
    }

    protected void setResourceGroupManager(ResourceGroupManager resourceGroupManager) {
        this.resourceGroupManager = resourceGroupManager;
    }

    protected void setMaterialManager(MaterialManager materialManager) {
        this.materialManager = materialManager;
    }

    public GpuProgramManager getGpuProgramManager() {
        return gpuProgramManager;
    }

    public HardwareBufferManager getHardwareBufferManager() {
        return hardwareBufferManager;
    }

    public SceneManager getSceneManager() {
        return sceneManager;
    }

    public TextureManager getTextureManager() {
        return textureManager;
    }

    public ResourceGroupManager getResourceGroupManager() {
        return resourceGroupManager;
    }

    public MaterialManager getMaterialManager() {
        return materialManager;
    }

    public RenderWindow getRenderWindow() {
        return renderWindow;
    }

    public void setRenderQueue(RenderQueue renderQueue) {
        this.renderQueue = renderQueue;
    }
    
    public void background(float r, float g, float b) throws JavaGraphicsException {
        background[0] = r;
        background[1] = g;
        background[2] = b;
    }
    
    public void background(Texture texture) throws JavaGraphicsException {
        throw new JavaGraphicsException(ExceptionType.NOT_IMPLEMENTED, "Method is not implemented.");
    }

    abstract public RenderWindow createRenderWindow(String name, int width, int height);
    abstract protected void renderImpl(RenderOperation ro) throws JavaGraphicsException;
    abstract protected void initializeImpl() throws JavaGraphicsException;
    
    /**
     * Очищает сцену
     */
    abstract public void clearScene();
    
    /**
     * Подвязывает текстуру к контексту
     * @param texture Текстура, которую необходимо подвязать к контексту
     */
    abstract public void bindTexture(Texture texture);
    
    /**
     * Отвязывает текстуру от контекста 
     * @param texture Текстура
     */
    abstract public void unbindTexture(Texture texture);
    
    /**
     * Включить или отключить текстуру в контексте устройства
     * @param flag true/false
     * @throws JavaGraphicsException
     */
    abstract public void enableTexture(boolean flag) throws JavaGraphicsException;
    
    /**
     * Мировая матрица
     * @param matrix Матрица траспонирования
     */
    abstract public void setWorldTransform(Matrix matrix);
    
    /**
     * Привязать к контексту устройсва вершинный буфер
     * @param buffer
     * @throws JavaGraphicsException
     */
    abstract public void bindHardwareVertexBuffer(HardwareVertexBuffer buffer) throws JavaGraphicsException;
    
    /**
     * Привязать к контексту устройсва GPU-программу (шейдеры)
     * @param program
     * @throws JavaGraphicsException
     */
    abstract public void bindGpuProgram(GpuProgram program) throws JavaGraphicsException;
    
    /**
     * Отвязать от контекста устройсва GPU-программу
     * @param program
     * @throws JavaGraphicsException
     */
    abstract public void unbindGpuProgram(GpuProgram program) throws JavaGraphicsException;
    
   
    abstract public void beginViewPort() throws JavaGraphicsException;
    abstract public void endViewPort() throws JavaGraphicsException;
    
    abstract public void setRotate(float angle);
    abstract public void setPosition(Vector2d position);

    abstract public void shutdown();
}
