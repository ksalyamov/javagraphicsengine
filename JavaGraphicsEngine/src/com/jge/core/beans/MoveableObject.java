package com.jge.core.beans;

import com.jge.core.exceptions.JavaGraphicsException;
import com.jge.core.interfaces.Moveable;

public abstract class MoveableObject implements Moveable {
    private String name;
    private boolean visible;
    private SceneNode node;

    @Override
    public void setName(String name) throws JavaGraphicsException {
        this.name = name;
    }

    @Override
    public String getName() throws JavaGraphicsException {
        return name;
    }

    @Override
    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    @Override
    public boolean isVisible() {
        return node == null ? false : node.isVisible();
    }

    @Override
    public boolean isAttached() {
        return getSceneNode() != null;
    }

    @Override
    public SceneNode getSceneNode() {
        return node;
    }

    @Override
    public void setSceneNode(SceneNode node) {
        this.node = node;
    }

}
