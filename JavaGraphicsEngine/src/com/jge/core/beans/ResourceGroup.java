package com.jge.core.beans;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ResourceGroup {
    private String name;
    private List<GroupLocation> locations;
    private List<Resource> resources;

    public ResourceGroup() {
        super();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<GroupLocation> getLocations() {
        if (locations == null) {
            locations = new ArrayList<GroupLocation>();
        }
        return locations;
    }

    public List<Resource> getResources() {
        if (resources == null) {
            resources = new ArrayList<Resource>();
        }
        return resources;
    }

    /**
     * Добавляет в пути. Не использовать.
     * @param directory
     * @param type
     */
    @Deprecated
    public void addFilesFromDirectory(File directory, String type) {
        if (!directory.isDirectory()) {
            // TODO предупреждение, что не является директорией
            return;
        }

        File[] files = directory.listFiles();

        for (File file : files) {
            if (!file.isFile()) continue;

            getLocations().add(new GroupLocation(file.getAbsolutePath(), type));
        }
    }

    public File getFile(String name) {
        File file = null;

        for (GroupLocation location : getLocations()) {
            file = location.getFile(name);
            if (file != null) {
                break;
            }
        }

        return file;
    }
}
