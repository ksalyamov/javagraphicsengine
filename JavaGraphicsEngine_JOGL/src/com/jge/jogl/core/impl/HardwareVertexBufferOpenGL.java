package com.jge.jogl.core.impl;

import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.Arrays;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;

import com.jge.core.enums.ExceptionType;
import com.jge.core.exceptions.JavaGraphicsException;
import com.jge.core.interfaces.HardwareBuffer;
import com.jge.core.interfaces.HardwareVertexBuffer;
import com.jge.core.managers.HardwareBufferManager;
import com.jge.jogl.beans.OpenGLMappings;
import com.jogamp.common.nio.Buffers;

public class HardwareVertexBufferOpenGL extends HardwareVertexBuffer {
    private RenderSystemOpenGL renderSystem;
    private int bufferId[];

    public HardwareVertexBufferOpenGL(RenderSystemOpenGL renderSystem, HardwareBufferManager manager,
            int vertexSize, int numVertices, HardwareBuffer.Usage usage) {
        super(manager, vertexSize, numVertices, usage);
        this.renderSystem = renderSystem;
        this.bufferId = new int[1];

        this.renderSystem.getGL2().glGenBuffers(1, bufferId, 0);
        this.renderSystem.getGL2().glBindBuffer(GL.GL_ARRAY_BUFFER, bufferId[0]);
        this.renderSystem.getGL2().glBufferData(GL2.GL_ARRAY_BUFFER, 0, null, OpenGLMappings.map(usage));
    }

    @Override
    public byte[] readData(int offset, int length) throws JavaGraphicsException {
        //throw new JavaGraphicsException(ExceptionType.NOT_IMPLEMENTED, "Method not implemented");
        renderSystem.getGL2().glBindBuffer(GL.GL_ARRAY_BUFFER, getBufferId()[0]);

        ByteBuffer buffer = Buffers.newDirectByteBuffer(length);
        renderSystem.getGL2().glGetBufferSubData(GL.GL_ARRAY_BUFFER, offset, length, buffer);

        byte[] result = new byte[length];
        buffer.get(result);
        return result;
    }

    @Override
    public void writeData(int offset, int length, float data[])
            throws JavaGraphicsException {
        if (data == null || data.length == 0) {
            // TODO exception
            throw new JavaGraphicsException("Bad data format");
        }
        renderSystem.getGL2().glBindBuffer(GL.GL_ARRAY_BUFFER, bufferId[0]);

        FloatBuffer buffer = Buffers.newDirectFloatBuffer(length);
        for (float b : data) {
            buffer.put(b);
        }
        buffer.rewind();

        if (offset == 0 && length == sizeInBytes) {
            renderSystem.getGL2().glBufferData(GL2.GL_ARRAY_BUFFER, sizeInBytes, buffer, OpenGLMappings.map(usage));
        } else {
            renderSystem.getGL2().glBufferSubData(GL2.GL_ARRAY_BUFFER, offset, length, buffer);
        }
    }

    public int[] getBufferId() {
        return bufferId;
    }
}
