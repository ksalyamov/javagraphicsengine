import java.io.File;

import com.jge.core.Root;
import com.jge.core.beans.Camera;
import com.jge.core.beans.Resource;
import com.jge.core.beans.SceneNode;
import com.jge.core.exceptions.JavaGraphicsException;
import com.jge.core.interfaces.RenderSystem;
import com.jge.core.managers.MaterialManager;
import com.jge.core.managers.ResourceGroupManager;
import com.jge.core.managers.SceneManager;
import com.jge.core.managers.TextureManager;
import com.jge.core.maths.Vector2d;
import com.jge.core.objects.Sprite;
import com.jge.core.objects.Texture;
import com.jge.core.shaders.GpuProgramParameter;
import com.jge.core.shaders.Material;


public class JGESimpleWindow extends BaseRenderedWindow {
    private SceneNode node;
    private Sprite sprite;
    private float speed = 50.0f;
    private float angle = 0;
    private Texture animation[];
    private float animationSpeed = 0;
    private Camera camera;

    protected JGESimpleWindow(String title, int width, int height)
            throws JavaGraphicsException {
        super(title, width, height);
    }

    @Override
    public void createScene() throws JavaGraphicsException {
        Root.loadResourcesFromFile(new File("media\\resources.xml"));
        

        // Регистрируем основы для спрайтов
        SceneManager.getInstance().registerBaseSprite("BaseSpriteDefault", 1, 1);
        SceneManager.getInstance().registerBaseSprite("BaseSprite100x100", 100, 100);
        SceneManager.getInstance().registerBaseSprite("BaseSprite200x200", 200, 200);

        // Создаем камеру, если она не была создана
        camera = SceneManager.getInstance().createOrRetriveCamera();
        Root.getInstance().getRenderedWindow().addViewport(camera);

        // Находим спрайт по имени
        //sprite = SceneManager.getInstance().getSprite("Sprite100x100");
        sprite = SceneManager.getInstance().createSprite("Sprite100x100", "BaseSprite100x100");
        // Добавляем в узел наш спрайт - узел отвечает за местоположение и масштабирование прикрепленного объекта
        node = SceneManager.getInstance().getRootSceneNode();
        node.attachObject(sprite);
        // Устанавливаем что объекты по текущему узлу располагаются по координатам (0;0)
        node.setPosition(0, 0);

        // Устанавливаем материал (способ рендеринга) для текущего спрайта
        Material material = (Material) MaterialManager.getInstance().getByName("Simple/Colored");
        sprite.setMaterial(material);

        this.animation = new Texture[17];
        for (int i=0; i<animation.length; i++) {
            String name = String.format("rockm00%02d.bmp", i);
            animation[i] = (Texture) TextureManager.getInstance().getByName(name);
        }

        // Создаем ещё один спрайт
        Sprite sprite2 = SceneManager.getInstance().createSprite("Sprite2", "BaseSprite200x200");
        SceneNode node2 = node.createChildNode();
        node2.setPosition(100, 250);
        node2.attachObject(sprite2);
        sprite2.setMaterial(material);
        sprite2.attachTexture(animation[0]);
        
        // Устанавливаем цвет фона
        RenderSystem.getInstance().background(1, 0, 0);
    }

    @Override
    public void updateScene(float elapsedTime) throws JavaGraphicsException {	
        if (node.getPosition().getX() > 100) {
            speed = -Math.abs(speed);
        } else if (node.getPosition().getX() < -100) {
            speed = Math.abs(speed);
        }

        Vector2d v = node.getPosition();
        angle += speed*elapsedTime;

        // Двигаем камеру вправо
        camera.move(Camera.Axis.X, Math.abs(speed*0.30f) * elapsedTime);

        node.setAngle(angle);
        node.setPosition(v.getX() + speed*elapsedTime, v.getY());

        sprite.attachTexture(animation[(int)animationSpeed]);
        animationSpeed += 30*elapsedTime;
        if (animationSpeed > 16)
            animationSpeed = 0.0f;
    }

    @Override
    public void destroyScene() throws JavaGraphicsException {
        // TODO Auto-generated method stub

    }

    public static void main(String[] args) throws JavaGraphicsException {
        JGESimpleWindow window = new JGESimpleWindow("Window", 800, 600);
    }

}
