package com.jge.core.beans;

public class RenderOperation {
    private VertexData vertexData;
    private RenderOperation.Type type;

    public RenderOperation() {
        super();
        this.vertexData = new VertexData();
    }

    public VertexData getVertexData() {
        return vertexData;
    }

    public void setVertexData(VertexData vertexData) {
        this.vertexData = vertexData;
    }

    public RenderOperation.Type getType() {
        return type;
    }

    public void setType(RenderOperation.Type type) {
        this.type = type;
    }

    public static enum Type {
        ROT_POINT_LIST,
        ROT_LINE_LIST,
        ROT_LINE_STRIP,
        ROT_TRIANGLE_LIST,
        ROT_TRIANGLE_STRIP,
        ROT_TRIANGLE_FAN
    }
}
