package com.jge.core.shaders;

import java.util.ArrayList;
import java.util.List;

import com.jge.core.exceptions.JavaGraphicsException;

public class Technique {
    private List<Pass> passes;
    private Material parentMaterial;

    public Technique(Material parentMaterial) throws JavaGraphicsException {
        super();
        this.parentMaterial = parentMaterial;
        this.passes = new ArrayList<Pass>();
    }

    public Pass createPass() throws JavaGraphicsException {
        Pass pass = new Pass(this);
        addPass(pass);
        return pass;
    }

    private void addPass(Pass pass) throws JavaGraphicsException {
        if (passes != null) {
            passes.add(pass);
        }
    }

    public void prepare() throws JavaGraphicsException {
        for (Pass pass : passes) {
            pass.prepare();
        }
    }

    public void load() throws JavaGraphicsException {
        for (Pass pass : passes) {
            pass.load();
        }
    }

    public void unload() throws JavaGraphicsException {
        for (Pass pass : passes) {
            pass.unload();
        }
    }

    public List<Pass> getPasses() throws JavaGraphicsException {
        return passes;
    }

    public Pass getPass(int index) throws JavaGraphicsException {
        return passes.get(index);
    }
}
