package com.jge.core.beans;

public class VertexElement {
    private short source;
    private long offset;
    private Type type;
    private Semantic semantic;
    private int index;

    public VertexElement(short source, long offset, VertexElement.Type type, VertexElement.Semantic semantic, int index) {
        super();
        this.offset = offset;
        this.type = type;
        this.semantic = semantic;
        this.source = source;
    }

    public VertexElement(short source, long offset, VertexElement.Type type, VertexElement.Semantic semantic) {
        this(source, offset, type, semantic, 0);
    }

    public long getOffset() {
        return this.offset;
    }

    public VertexElement.Type getType() {
        return this.type;
    }

    public VertexElement.Semantic getSemantic() {
        return this.semantic;
    }

    public int getSize() {
        return type != null ? type.getSize() : 0;
    }

    public short getSource() {
        return source;
    }

    public int getIndex() {
        return index;
    }

    public static enum Type {
        VET_FLOAT(Float.SIZE/Byte.SIZE, 1),
        VET_FLOAT1(VET_FLOAT.getSize(), VET_FLOAT.getCount()),
        VET_FLOAT2(VET_FLOAT.getSize(), 2),
        VET_FLOAT3(VET_FLOAT.getSize(), 3),
        VET_FLOAT4(VET_FLOAT.getSize(), 4),

        VET_SHORT(Short.SIZE/Byte.SIZE, 1),
        VET_SHORT1(Short.SIZE/Byte.SIZE, 1),
        VET_SHORT2(Short.SIZE/Byte.SIZE, 2),
        VET_SHORT3(Short.SIZE/Byte.SIZE, 3),
        VET_SHORT4(Short.SIZE/Byte.SIZE, 4),

        VET_COLOUR(Float.SIZE/Byte.SIZE, 4),
        VET_COLOUR_ABGR(Integer.SIZE/Byte.SIZE, 1),
        VET_COLOUR_ARGB(Integer.SIZE/Byte.SIZE, 1),
        VET_UBYTE4(Short.SIZE/Byte.SIZE, 4);

        int size;
        int count;

        Type(int size, int count) {
            this.size = size;
            this.count = count;
        }

        public int getSize() {
            return size*count; 
        }

        public int getCount() {
            return count;
        }
    }

    public static enum Semantic {
        VES_POSITION,
        VES_NORMAL,
        VES_DIFFUSE,
        VES_TEXTURE_COORDINATES;
    }
}