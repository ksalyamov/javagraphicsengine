package com.jge.core.interfaces;

import com.jge.core.enums.LoadingStatus;

/**
 * 
 * @author Салямов Кирилл
 *
 */
public interface StatedItem {
    /**
     * Устанавливает статус загрузки
     * @param status Статус
     */
    void setStatus(LoadingStatus status);

    /**
     * Возвращает статус загрузки
     * @return Статус
     */
    LoadingStatus getStatus();
}
