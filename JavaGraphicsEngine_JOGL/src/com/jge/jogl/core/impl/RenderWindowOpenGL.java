package com.jge.jogl.core.impl;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.GLProfile;
import javax.media.opengl.awt.GLCanvas;
import javax.swing.JFrame;
import com.jge.core.beans.Camera;
import com.jge.core.exceptions.JavaGraphicsException;
import com.jge.core.interfaces.RenderSystem;
import com.jge.core.interfaces.RenderWindow;
import com.jge.core.managers.ResourceGroupManager;
import com.jge.core.managers.SceneManager;
import com.jogamp.opengl.util.Animator;

public class RenderWindowOpenGL extends RenderWindow {
    private GLCanvas canvas;
    private Graphics frame;
    private int windowWidth;
    private int windowHeight;

    @Override
    public void create(String name, int width, int height) {
        setName(name);
        setWidth(width);
        setHeight(height);

        // TODO
        frame = new Graphics(this);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }

    @Override
    public void resize(int width, int height) {
        setWidth(width);
        setHeight(height);
    }

    @Override
    public void windowResized() {
        // TODO Auto-generated method stub

    }

    @Override
    public void swapBuffers(boolean flag) {
        if (flag) {
            canvas.swapBuffers();
        }
    }

    @Override
    public void beginUpdate() {
        // TODO Auto-generated method stub

    }

    @Override
    public void updateViewport() {
        // TODO Auto-generated method stub

    }

    @Override
    public void endUpdate() {
        // TODO Auto-generated method stub

    }

    public GLCanvas getGLCanvas() {
        return canvas;
    }

    private class Graphics extends JFrame implements GLEventListener, WindowListener{
        private RenderWindow rw;

        public Graphics(RenderWindow rw) {
            super();
            this.rw = rw;
            super.setTitle(rw.getName());
            super.setSize(rw.getWidth(), rw.getHeight());

            canvas = new GLCanvas(new GLCapabilities(null));
            canvas.addGLEventListener(this);
            getContentPane().add(canvas);
            addWindowListener(this);

            //Create an Animator linked to the Canvas
            Animator animator = new Animator(canvas);
            animator.start();
        }

        private void updateViewport(GL2 gl) {
            gl.glViewport(0, 0, windowWidth, windowHeight);

            gl.glMatrixMode(GL2.GL_PROJECTION);
            gl.glLoadIdentity();
            float camPosX = 0.0f;
            float camPosY = 0.0f;

            //gl.glOrtho(-width*0.5, width*0.5, -height*0.5, height*0.5, -1, 1);
            if (SceneManager.getInstance().isCameraCreated()) {
                /*Camera camera = SceneManager.getInstance().createOrRetriveCamera();
                camPosX = camera.getX();
                camPosY = camera.getY();*/
            }

            gl.glOrtho(camPosX - windowWidth*0.5, 
                    camPosX + windowWidth*0.5, 
                    camPosY - windowHeight*0.5, 
                    camPosY + windowHeight*0.5, 
                    -1, 
                    1);
            gl.glMatrixMode(GL2.GL_MODELVIEW);
        }

        @Override
        public void display(GLAutoDrawable arg0) {
            try {
                updateViewport((GL2) arg0.getGL());
                
                rw.update();
            } catch (JavaGraphicsException e) {
                // TODO Сделать нормальное человеческое логирование
                e.printStackTrace();
                sendWindowClosingEvent();
            }

        }

        @Override
        public void dispose(GLAutoDrawable arg0) {
            // TODO Auto-generated method stub

        }

        @Override
        public void init(GLAutoDrawable arg0) {
            try {
                SceneManager.getInstance().initializeScene();
            } catch (JavaGraphicsException e) {
                // TODO Сделать нормальное человеческое логирование
                e.printStackTrace();
                sendWindowClosingEvent();
            }
        }

        @Override
        public void reshape(GLAutoDrawable drawable, int x, int y, int width,
                int height) {
            windowWidth = width;
            windowHeight = height;

            updateViewport((GL2) drawable.getGL());

            rw.resize(width, height);
            rw.windowResized();
        }

        @Override
        public void windowActivated(WindowEvent arg0) {
            // TODO Auto-generated method stub

        }

        @Override
        public void windowClosed(WindowEvent arg0) {

        }

        @Override
        public void windowClosing(WindowEvent arg0) {
            try {
                rw.destroy();
            } catch (JavaGraphicsException e) {
                // TODO Сделать нормальное человеческое логирование
                e.printStackTrace();
            }

        }

        @Override
        public void windowDeactivated(WindowEvent arg0) {
            // TODO Auto-generated method stub

        }

        @Override
        public void windowDeiconified(WindowEvent arg0) {
            // TODO Auto-generated method stub

        }

        @Override
        public void windowIconified(WindowEvent arg0) {
            // TODO Auto-generated method stub

        }

        @Override
        public void windowOpened(WindowEvent arg0) {
            // TODO Auto-generated method stub

        }

        public void sendWindowClosingEvent() {
            processWindowEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING));
        }
    }

}
