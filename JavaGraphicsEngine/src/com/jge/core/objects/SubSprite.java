package com.jge.core.objects;

import java.util.List;

import com.jge.core.beans.RenderOperation;
import com.jge.core.beans.VertexBufferBinding;
import com.jge.core.beans.VertexDeclaration;
import com.jge.core.beans.VertexElement;
import com.jge.core.beans.RenderOperation.Type;
import com.jge.core.exceptions.JavaGraphicsException;
import com.jge.core.interfaces.HardwareBuffer;
import com.jge.core.interfaces.HardwareVertexBuffer;
import com.jge.core.interfaces.Moveable;
import com.jge.core.interfaces.RenderSystem;
import com.jge.core.interfaces.Renderable;
import com.jge.core.managers.HardwareBufferManager;
import com.jge.core.maths.Matrix;
import com.jge.core.shaders.Material;
import com.jge.core.shaders.Pass;
import com.jge.core.shaders.Technique;

public class SubSprite implements Renderable {
    private RenderOperation renderOperation;
    protected Material material;
    private Texture texture;
    private Moveable moveable;
    private float width;
    private float height;
    private String name;

    public SubSprite(String name, float width, float height) throws JavaGraphicsException {
        super();
        this.name = name;
        this.width = width;
        this.height = height;

        createVertexBuffer();
    }

    @Override
    public Matrix getWorldTransform() throws JavaGraphicsException {
        return moveable.getSceneNode().getNodeTransform();
    }

    public void setMoveable(Moveable moveable) {
        this.moveable = moveable;
    }

    @Override
    public void preRender(RenderSystem renderSystem, Pass pass)
            throws JavaGraphicsException {
        RenderSystem rs = RenderSystem.getInstance();
        rs.beginViewPort();

        if (pass != null) {
            if (pass.getVertexProgramUsage() != null && pass.getVertexProgramUsage().getGpuProgram() != null) {
                //rs.bindGpuProgram(pass.getVertexProgramUsage().getGpuProgram());
                pass.getVertexProgramUsage().getGpuProgram().bindProgram();
            }

            if (pass.getPixelProgramUsage() != null && pass.getPixelProgramUsage().getGpuProgram() != null) {
                //rs.bindGpuProgram(pass.getPixelProgramUsage().getGpuProgram());
                pass.getPixelProgramUsage().getGpuProgram().bindProgram();
            }
        }

        if (isTextureAttached())
            rs.bindTexture(getTexture());
        else
            rs.enableTexture(false);
    }

    @Override
    public void postRender(RenderSystem renderSystem, Pass pass)
            throws JavaGraphicsException {
        RenderSystem rs = RenderSystem.getInstance();
        if (pass != null) {
            if (pass != null) {
                if (pass.getPixelProgramUsage() != null && pass.getPixelProgramUsage().getGpuProgram() != null) {
                    //rs.unbindGpuProgram(pass.getPixelProgramUsage().getGpuProgram());
                    pass.getPixelProgramUsage().getGpuProgram().unbindProgram();
                }

                if (pass.getVertexProgramUsage() != null && pass.getVertexProgramUsage().getGpuProgram() != null) {
                    //rs.unbindGpuProgram(pass.getVertexProgramUsage().getGpuProgram());
                    pass.getVertexProgramUsage().getGpuProgram().unbindProgram();
                }
            }
        }
        if (isTextureAttached())
            rs.unbindTexture(getTexture());
        rs.endViewPort();
    }

    @Override
    public RenderOperation getRenderOperation() throws JavaGraphicsException {
        return renderOperation;
    }

    @Override
    public List<Technique> getTechniques() throws JavaGraphicsException {
        if (material != null) {
            return material.getTechniques();
        }
        return null;
    }

    @Override
    public Technique getTechnique() throws JavaGraphicsException {
        return material != null ? material.getTechnique(0) : null;
    }

    public void setTexture(Texture texture) {
        this.texture = texture;
    }

    public void setMaterial(Material material) {
        this.material = material;
    }

    public boolean isTextureAttached() {
        return texture == null ? false : (texture.isLoaded());
    }

    public Texture getTexture() {
        return texture;
    }

    private void createVertexBuffer() throws JavaGraphicsException {
        VertexDeclaration decl = HardwareBufferManager.getInstance().createVertexDeclaration();	

        int vertexCount = 6;
        long offset = 0;
        short source = 0;

        // Устанавливаем формат вершин
        decl.addVertex(source, offset, VertexElement.Type.VET_FLOAT3, VertexElement.Semantic.VES_POSITION);
        offset += VertexElement.Type.VET_FLOAT3.getSize();
        //decl.addVertex(source, offset, VertexElement.Type.VET_COLOUR, VertexElement.Semantic.VES_DIFFUSE);
        //offset += VertexElement.Type.VET_COLOUR.getSize();

        float height2 = getHeight() * 0.5f;
        float width2 = getWidth() * 0.5f;

        /*
         * 1--2 4
         * | / /|
         * |/ / |
         * 0 3--5
         *  
         */

        float data[] = new float[] {
                -width2, -height2, 0, 	// Координата (вершина 0) 
                //1, 0, 0, 0,				// Цвет 

                -width2, height2, 0,	// Координата (вершина 1) 
                //0, 1, 0, 0,				// Цвет 

                width2, height2, 0,		// Координата (вершина 2) 
                //0, 0, 1, 0,				// Цвет 

                -width2, -height2, 0,	// Координата (вершина 3) 
                //1, 0, 0, 0,				// Цвет 

                width2, height2, 0,		// Координата (вершина 4) 
                ///0, 0, 1, 0,				// Цвет 

                width2, -height2, 0,	// Координата (вершина 5) 
                //0, 1, 0, 0,				// Цвет 
        };

        HardwareVertexBuffer vertexBuffer =  HardwareBufferManager.getInstance().createVertexBuffer((int)offset, 
                vertexCount, 
                HardwareBuffer.Usage.HBU_DYNAMIC_WRITE_ONLY);
        // Записываем данные текстуры
        vertexBuffer.writeData(0, (int)vertexBuffer.getSizeInBytes(), data);
        // Присоединяем созданный вершинный буфер
        VertexBufferBinding bind = HardwareBufferManager.getInstance().createVertexBufferBindings();
        bind.setBinding(source, vertexBuffer);

        data = new float []{
                0, 1,
                0, 0,
                1, 0,

                0, 1,
                1, 0,
                1, 1,
        };
        offset = 0;
        source = 1;
        decl.addVertex(source, offset, VertexElement.Type.VET_FLOAT2, VertexElement.Semantic.VES_TEXTURE_COORDINATES);
        offset += VertexElement.Type.VET_FLOAT2.getSize();
        vertexBuffer =  HardwareBufferManager.getInstance().createVertexBuffer(
                (int)offset, 
                vertexCount, 
                HardwareBuffer.Usage.HBU_DYNAMIC_WRITE_ONLY);
        vertexBuffer.writeData(0, (int)vertexBuffer.getSizeInBytes(), data);
        bind.setBinding(source, vertexBuffer);

        // Создаём параметры на рендеринг
        renderOperation = new RenderOperation();
        renderOperation.setType(Type.ROT_TRIANGLE_LIST);
        renderOperation.getVertexData().setVertexBufferBinding(bind);
        renderOperation.getVertexData().setVertexCount(vertexCount);
        renderOperation.getVertexData().setVertexDeclaration(decl);
    }

    public float getWidth() {
        return width;
    }

    public float getHeight() {
        return height;
    }
}

