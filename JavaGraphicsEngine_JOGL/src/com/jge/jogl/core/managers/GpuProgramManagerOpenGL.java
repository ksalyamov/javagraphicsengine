package com.jge.jogl.core.managers;

import com.jge.core.beans.Resource;
import com.jge.core.exceptions.JavaGraphicsException;
import com.jge.core.interfaces.GpuProgram;
import com.jge.core.interfaces.GpuProgram.Type;
import com.jge.core.managers.GpuProgramManager;
import com.jge.core.managers.ResourceManager;
import com.jge.jogl.core.impl.GpuProgramOpenGL;

public class GpuProgramManagerOpenGL extends GpuProgramManager {

    public GpuProgramManagerOpenGL()
            throws JavaGraphicsException {
        super();

    }

    @Override
    protected GpuProgram createImpl(String name, String groupName, Type type)
            throws JavaGraphicsException {

        return new GpuProgramOpenGL(this, name, groupName, type);
    }

    @Override
    protected Resource createImpl(ResourceManager creator, String name,
            String group) throws JavaGraphicsException {
        System.out.println("There is no");
        return null;
    }

}
