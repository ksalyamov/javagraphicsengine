package com.jge.core.beans;

import java.util.logging.Level;
import java.util.logging.Logger;

import com.jge.core.enums.LoadingStatus;
import com.jge.core.exceptions.JavaGraphicsException;
import com.jge.core.interfaces.TaskItem;
import com.jge.core.managers.ResourceManager;

public abstract class Resource implements TaskItem {
    private static Logger logger = Logger.getGlobal();

    private String name;
    private LoadingStatus status;
    private String groupName;
    private ResourceManager creator;

    protected Resource(ResourceManager creator, String name, String groupName) {
        super();
        this.creator = creator;
        this.name = name;
        this.groupName = groupName;
        this.status = LoadingStatus.NONE;
    }

    protected void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public ResourceManager getCreator() {
        return creator;
    }

    @Override
    public void setStatus(LoadingStatus status) {
        this.status = status;
    }

    @Override
    public LoadingStatus getStatus() {
        return this.status;
    }

    @Override
    public String info() throws JavaGraphicsException {
        return "";
    }

    public boolean isLoaded() {
        return getStatus() == LoadingStatus.LOADED;
    }

    public boolean isUnloaded() {
        return getStatus() == LoadingStatus.UNLOADED;
    }

    public void prepare() throws JavaGraphicsException {
        try {
            logger.info(String.format("Preparing resource(%s) \"%s\"", getClass().getName(), getName()));
            setStatus(LoadingStatus.PREPARING);
            prepareImpl();
            setStatus(LoadingStatus.PREPARED);
        } catch (JavaGraphicsException e) {
            setStatus(LoadingStatus.UNLOADED);
            throw e;
        } finally {
        }
    }

    public void load() throws JavaGraphicsException {
        try {
            logger.info(String.format("Loading resource(%s) \"%s\"", getClass().getName(), getName()));
            loadImpl();
            setStatus(LoadingStatus.LOADED);
        } catch (JavaGraphicsException e) {
            setStatus(LoadingStatus.UNLOADED);
            throw e;
        } finally {

        }
    }

    public void unload() throws JavaGraphicsException {
        try {
            unloadImpl();
        } finally {
            logger.info(String.format("Unload resource(%s) \"%s\"", getClass().getName(), getName()));
            setStatus(LoadingStatus.UNLOADED);
        }
    }

    abstract protected void prepareImpl() throws JavaGraphicsException;
    abstract protected void loadImpl() throws JavaGraphicsException;
    abstract protected void unloadImpl() throws JavaGraphicsException;
}
