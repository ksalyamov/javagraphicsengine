package com.jge.core.beans;

import com.jge.core.exceptions.JavaGraphicsException;
import com.jge.core.interfaces.GpuProgram;
import com.jge.core.managers.GpuProgramManager;
import com.jge.core.shaders.GpuProgramParameter;
import com.jge.core.shaders.Pass;

public class GpuProgramUsage {
    private GpuProgram.Type type;
    private Pass parentPass;
    private GpuProgram gpuProgram;

    public GpuProgramUsage(GpuProgram.Type type, Pass pass) throws JavaGraphicsException {
        super();
        this.type = type;
        this.parentPass = pass;
    }

    public void setProgram(String name) throws JavaGraphicsException {
        setProgram((GpuProgram) GpuProgramManager.getInstance().getByName(name));
    }

    public void setProgram(GpuProgram program) throws JavaGraphicsException {
        this.gpuProgram = program;

        if (gpuProgram.getType() != type) {
            throw new JavaGraphicsException(String.format("Bad type of shader. It should be \"%s\", but it is \"%s\"", type, gpuProgram.getType()));
        }
    }

    public void load() throws JavaGraphicsException {
        if (gpuProgram != null) {
            gpuProgram.load();
        }
    }

    public void unload() throws JavaGraphicsException {

    }

    public GpuProgram.Type getType() {
        return type;
    }

    public GpuProgram getGpuProgram() {
        return gpuProgram;
    }

    public GpuProgramParameter createParameter(String name, GpuProgramParameter.Type type) throws JavaGraphicsException {
        return parentPass.createParameter(name, type);
    }
}
