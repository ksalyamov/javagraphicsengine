package com.jge.jogl.core;

import com.jge.core.Root;
import com.jge.core.exceptions.JavaGraphicsException;
import com.jge.core.interfaces.ThreadedLoadingQueue;
import com.jge.core.managers.MaterialManager;
import com.jge.core.managers.ResourceGroupManager;
import com.jge.jogl.core.impl.RenderSystemOpenGL;
import com.jge.jogl.core.impl.RenderWindowOpenGL;
import com.jge.jogl.core.managers.GpuProgramManagerOpenGL;
import com.jge.jogl.core.managers.HardwareBufferManagerOpenGL;
import com.jge.jogl.core.managers.SceneManagerOpenGL;
import com.jge.jogl.core.managers.TextureManagerOpenGL;

public class JgeInitialize {
    private static Root root;
    public static Root initialize() throws JavaGraphicsException {
        root = new Root();
        root.setRenderSystem(new RenderSystemOpenGL());		
        return root;
    }
}
