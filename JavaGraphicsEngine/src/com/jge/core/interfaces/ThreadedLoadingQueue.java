package com.jge.core.interfaces;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import com.jge.core.beans.Task;
import com.jge.core.enums.LoadingStatus;
import com.jge.core.exceptions.JavaGraphicsException;

public class ThreadedLoadingQueue implements LoadingQueue, Runnable{
    private AtomicBoolean running;
    private List<Task> tasks;
    private JavaGraphicsException fatalException;

    public ThreadedLoadingQueue() {
        super();
        tasks = Collections.synchronizedList(new ArrayList<Task>());
        Thread thread = new Thread(this);

        this.running = new AtomicBoolean();
        this.running.set(true);
        thread.start();
    }

    @Override
    public void addTask(Task task) {
        for (Task t : tasks) {
            if (t.getTaskItem() == task.getTaskItem())
                return;
        }
        task.setBackgroundLoading(true);
        tasks.add(task);
    }

    @Override
    public void removeTask(Task task) {
        tasks.remove(task);
    }

    @Override
    public Task getTaskAndRemove() {
        return null;
    }

    @Override
    public Task getTask() {
        Task task = null;
        for (int i=0; i<tasks.size(); i++) {
            try {
                Task t = tasks.get(i);
                if (t.getTaskItem().getStatus() == LoadingStatus.NONE) {
                    task = t;
                    break;
                }
            } catch (IndexOutOfBoundsException e) {
                break;
            }
        }

        return task;
    }

    @Override
    public Task getTaskCreated() {
        Task task = null;
        for (Task t : tasks) {
            if (t.getTaskItem().getStatus() == LoadingStatus.PREPARED) {
                task = t;
                break;
            }
        }
        return task;
    }

    @Override
    public int size() {
        return tasks.size();
    }

    @Override
    public boolean isEmtpy() {
        return tasks.isEmpty();
    }

    @Override
    public void loading() throws JavaGraphicsException {
        if (fatalException != null && !this.running.get()) {
            throw fatalException;
        }

        Task task = getTaskCreated();
        if (task == null) return;
        task.loading();
        removeTask(task);
    }

    @Override
    public void run() {
        while (running.get()) {

            while (!tasks.isEmpty() && running.get()) {
                Task task = getTask();
                if (task == null) continue;

                try {
                    task.preparing();
                } catch (JavaGraphicsException e) {
                    shutdown();
                    // TODO LOGGER
                    fatalException = e;
                }
                if (task.getTaskItem().getStatus() == LoadingStatus.UNLOADED) {
                    removeTask(task);
                } else if (task.getTaskItem().getStatus() == LoadingStatus.LOADED) {
                    task.setBackgroundLoading(false);
                }
                break;
            }
        }
    }

    public void shutdown() {
        this.running.set(false);
    }
}
