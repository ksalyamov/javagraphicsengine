package com.jge.core.shaders;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.jge.core.beans.Resource;
import com.jge.core.beans.ResourceGroup;
import com.jge.core.beans.Task;
import com.jge.core.enums.ExceptionType;
import com.jge.core.exceptions.JavaGraphicsException;
import com.jge.core.interfaces.GpuProgram;
import com.jge.core.managers.GpuProgramManager;
import com.jge.core.managers.ResourceGroupManager;
import com.jge.core.managers.ResourceManager;

public class Material extends Resource {
    private File file;
    private List<Technique> techniques;

    public Material(ResourceManager creator, String name, String groupName) throws JavaGraphicsException {
        super(creator, name, groupName);
        this.techniques = new ArrayList<>();

        ResourceGroup group = ResourceGroupManager.getInstance().getResourceGroup(getGroupName());
        file = group.getFile(name);
        if (file == null) {
            // TODO JavaGraphicsException, фатальный, невозможно найти ресурс по name в указанных путях
        }
    }

    public List<Technique> getTechniques() {
        return techniques;
    }

    public void setTechniques(List<Technique> techniques) {
        this.techniques = techniques;
    }

    public void addTechnique(Technique technique) throws JavaGraphicsException {
        if (technique != null) {
            techniques.add(technique);
        }
    }

    public Technique createTechnique() throws JavaGraphicsException {
        Technique technique = new Technique(this);
        addTechnique(technique);
        return technique;
    }

    @Override
    public void preparing() throws JavaGraphicsException {		

    }

    @Override
    public void loading() throws JavaGraphicsException {
        createMaterialFromXML();
        for (Technique technique : techniques) {
            technique.load();
        }
    }

    private void createMaterialFromXML() throws JavaGraphicsException {
        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;

        try {
            builder = builderFactory.newDocumentBuilder();

            Document document = builder.parse(new FileInputStream(file));
            XPath xPath =  XPathFactory.newInstance().newXPath();

            Node node= (Node) xPath.compile("/material").evaluate(document, XPathConstants.NODE);
            setName(node.getAttributes().getNamedItem("name").getNodeValue()); // TODO

            NodeList nodeList = null;

            // Загружаем все шейдеры из файла
            // Вершинные шейдеры
            nodeList = (NodeList) xPath.compile("/material/vertex_shader").evaluate(document, XPathConstants.NODESET);
            for (int i = 0; nodeList != null && i < nodeList.getLength(); i++) {
                Node shader = nodeList.item(i);
                String name = shader.getAttributes().getNamedItem("name").getNodeValue();
                String source = (String) xPath.compile(String.format("/material/vertex_shader[%d]/source", i+1)).evaluate(document, XPathConstants.STRING);

                // Создаем и добавляем в очередь на загрузку
                GpuProgram gpuProgram = GpuProgramManager.getInstance().createProgram(name, GpuProgram.Type.GPT_VERTEX_SHADER, source);
            }			
            // Пиксельные шейдеры
            nodeList = (NodeList) xPath.compile("/material/pixel_shader").evaluate(document, XPathConstants.NODESET);
            for (int i = 0; nodeList != null && i < nodeList.getLength(); i++) {
                Node shader = nodeList.item(i);
                String name = shader.getAttributes().getNamedItem("name").getNodeValue();
                String source = (String) xPath.compile(String.format("/material/pixel_shader[%d]/source", i+1)).evaluate(document, XPathConstants.STRING);

                // Создаем и добавляем в очередь на загрузку
                GpuProgram gpuProgram = GpuProgramManager.getInstance().createProgram(name, GpuProgram.Type.GPT_PIXEL_SHADER, source);
            }

            // Создаем и регистрируем все техники прохода
            NodeList techniques = (NodeList) xPath.compile("/material/technique").evaluate(document, XPathConstants.NODESET);
            for (int i = 0; techniques != null && i < techniques.getLength(); i++) {
                NodeList passes = (NodeList) xPath.compile(String.format("/material/technique[%d]/pass", i+1)).evaluate(document, XPathConstants.NODESET);

                Technique technique = createTechnique();

                for (int j = 0; passes != null && j < passes.getLength(); j++) {
                    String path = String.format("/material/technique[%d]/pass[%d]", i+1, j+1);
                    Node vs = (Node) xPath.compile(String.format("%s/vertex_shader", path)).evaluate(document, XPathConstants.NODE);
                    Node ps = (Node) xPath.compile(String.format("%s/pixel_shader", path)).evaluate(document, XPathConstants.NODE);

                    Pass pass = technique.createPass();
                    GpuProgram program = null;

                    String vertexShaderName = (vs.getAttributes().getNamedItem("name").getNodeValue());
                    program = (GpuProgram) GpuProgramManager.getInstance().getByName(vertexShaderName);
                    pass.setVertexProgram(vertexShaderName);

                    String pixelShaderName = (ps.getAttributes().getNamedItem("name").getNodeValue());
                    pass.setPixelProgram(pixelShaderName);
                }
            }
        } catch (ParserConfigurationException e) {
            throw new JavaGraphicsException(ExceptionType.DEFAULT, e);
        } catch (FileNotFoundException e) {
            throw new JavaGraphicsException(ExceptionType.FILE_NOT_FOUND, e);
        } catch (SAXException e) {
            throw new JavaGraphicsException(ExceptionType.DEFAULT, e);
        } catch (IOException e) {
            throw new JavaGraphicsException(ExceptionType.DEFAULT, e);
        } catch (XPathExpressionException e) {
            throw new JavaGraphicsException(ExceptionType.DEFAULT, e);
        }

        System.out.printf("Material \"%s\" is creating\n", this.getName());
        System.out.printf("\t%d techniques, %d passes\n", this.getTechniques().size(), 0);
    }

    @Override
    public void loadImpl() throws JavaGraphicsException {
        createMaterialFromXML();
        for (Technique technique : techniques) {
            technique.load();
        }
    }

    @Override
    public void unloadImpl() throws JavaGraphicsException {
        for (Technique technique : techniques) {
            technique.unload();
        }
    }

    public Technique getTechnique(int index) throws JavaGraphicsException {
        if (index >= 0 && this.techniques != null && index <= this.techniques.size() - 1) {
            return this.techniques.get(index);
        }
        return null;
    }

    @Override
    protected void prepareImpl() throws JavaGraphicsException {


    }
}
