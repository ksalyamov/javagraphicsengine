package com.jge.core.objects;


import com.jge.core.beans.MoveableObject;
import com.jge.core.exceptions.JavaGraphicsException;
import com.jge.core.interfaces.RenderQueue;
import com.jge.core.shaders.Material;


/**
 * 
 * @author Salyamov Kirill
 *
 */
public class Sprite extends MoveableObject {
    private String name;
    protected Material material;
    protected SubSprite subSprite;
    private Texture texture;

    public Sprite(String name, SubSprite subSprite) {
        super();
        this.name = name; 
        this.subSprite = subSprite;
    }

    @Override
    public void updateRenderQueue(RenderQueue queue)
            throws JavaGraphicsException {
        subSprite.setMoveable(this);
        subSprite.setMaterial(material);
        subSprite.setTexture(texture);
        queue.addRenderable(subSprite);
    }

    public void attachTexture(Texture texture) {
        this.texture = texture;
    }

    public void setMaterial(Material material) {
        this.material = material;
    }
}