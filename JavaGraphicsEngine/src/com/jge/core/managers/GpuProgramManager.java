package com.jge.core.managers;

import java.util.concurrent.Semaphore;

import com.jge.core.beans.Resource;
import com.jge.core.beans.Task;
import com.jge.core.enums.ExceptionType;
import com.jge.core.enums.ResourceGroupName;
import com.jge.core.exceptions.JavaGraphicsException;
import com.jge.core.interfaces.GpuProgram;

public abstract class GpuProgramManager extends ResourceManager {
    private static GpuProgramManager instance;
    private Semaphore mutex;

    private static void setInstance(GpuProgramManager gpuProgramManager) throws JavaGraphicsException {
        GpuProgramManager.instance = gpuProgramManager;
    }

    public static GpuProgramManager getInstance() {
        return instance;
    }

    protected GpuProgramManager() throws JavaGraphicsException {
        super(ResourceGroupName.SHADERS.getName());
        GpuProgramManager.setInstance(this);
        //ResourceGroupManager.getInstance().unregisterResourceManager(this);

        this.mutex = new Semaphore(1);
    }

    public GpuProgram createProgram(String name, GpuProgram.Type type, String filename) throws JavaGraphicsException {
        try {
            mutex.acquire();

            GpuProgram program = create(name, getGroupName(), type);

            program.setType(type);
            program.setSourceFile(filename);
            return program;

        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new JavaGraphicsException(ExceptionType.THREAD_INTERRUPTED, e);
        } finally {
            mutex.release();
        }
    }

    public GpuProgram createProgramFromString(String name, GpuProgram.Type type, String code) throws JavaGraphicsException {
        GpuProgram program = create(name, getGroupName(), type);

        program.setType(type);
        program.setSource(code);
        return program;
    }

    private GpuProgram create(String name, String groupName, GpuProgram.Type type) throws JavaGraphicsException {
        GpuProgram resource = createImpl(name, groupName, type);
        addImpl (resource);
        ResourceGroupManager.getInstance().notifyResourceCreated(resource);
        return resource;
    }

    protected abstract GpuProgram createImpl(String name, String groupName, GpuProgram.Type type) throws JavaGraphicsException; 

    public Resource load(String name, GpuProgram.Type type, String filename) throws JavaGraphicsException {
        Resource resource = createProgram(name, type, filename);
        ResourceGroupManager.getInstance().getLoadingQueue().addTask(new Task(resource));
        return resource;
    }

    public Resource loadFromString(String name, GpuProgram.Type type, String code) throws JavaGraphicsException {
        Resource resource = createProgramFromString(name, type, code);
        ResourceGroupManager.getInstance().getLoadingQueue().addTask(new Task(resource));
        return resource;
    }
}
