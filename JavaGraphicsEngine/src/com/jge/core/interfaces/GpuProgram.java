package com.jge.core.interfaces;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Scanner;

import com.jge.core.beans.Resource;
import com.jge.core.exceptions.JavaGraphicsException;
import com.jge.core.managers.ResourceManager;
import com.jge.core.shaders.GpuProgramParameter;

public abstract class GpuProgram extends Resource {
    private GpuProgram.Type type;
    private String filename;
    private String source;

    protected GpuProgram(ResourceManager creator, String name, String groupName, GpuProgram.Type type) {
        super(creator, name, groupName);
        this.type = type;
    }

    public GpuProgram.Type getType() {
        return type;
    }

    public void setType(GpuProgram.Type type) {
        this.type = type;
    }

    public String getSourceFile() {
        return filename;
    }

    public void setSourceFile(String filename) {
        this.filename = filename;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public void bindProgramParameters(List<GpuProgramParameter> parameters) throws JavaGraphicsException {
        for (GpuProgramParameter parameter : parameters) {
            bindProgramParameter(parameter);
        }
    }

    public void bindProgramParameter(GpuProgramParameter parameter) throws JavaGraphicsException {
        bindProgramParameterImpl(parameter);
    }

    abstract public void bindProgram() throws JavaGraphicsException;
    abstract public void unbindProgram() throws JavaGraphicsException;
    abstract protected void bindProgramParameterImpl(GpuProgramParameter parameter) throws JavaGraphicsException;

    public static enum Type {
        GPT_VERTEX_SHADER,
        GPT_PIXEL_SHADER
    }

    protected static String readFromStream(InputStream ins) throws IOException {
        if (ins == null) {
            throw new IOException("Cound not read from stream.");
        }
        StringBuffer buffer = new StringBuffer();
        Scanner scanner = new Scanner(ins);
        try {
            while (scanner.hasNextLine()) {
                buffer.append(scanner.nextLine() + "\n");
            }
        } finally {
            scanner.close();
        }
        return buffer.toString();

    }
}
