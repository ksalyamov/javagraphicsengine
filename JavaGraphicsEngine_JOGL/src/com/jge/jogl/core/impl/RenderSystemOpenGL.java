package com.jge.jogl.core.impl;

import java.nio.Buffer;
import java.nio.FloatBuffer;

import javax.media.opengl.GL;
import javax.media.opengl.GL2;
import javax.media.opengl.GLProfile;

import com.jge.core.Root;
import com.jge.core.beans.Camera;
import com.jge.core.beans.RenderOperation;
import com.jge.core.beans.SceneNode;
import com.jge.core.beans.VertexElement;
import com.jge.core.exceptions.JavaGraphicsException;
import com.jge.core.interfaces.GpuProgram;
import com.jge.core.interfaces.HardwareVertexBuffer;
import com.jge.core.interfaces.RenderQueue;
import com.jge.core.interfaces.RenderSystem;
import com.jge.core.interfaces.RenderWindow;
import com.jge.core.interfaces.ThreadedLoadingQueue;
import com.jge.core.managers.MaterialManager;
import com.jge.core.managers.ResourceGroupManager;
import com.jge.core.managers.SceneManager;
import com.jge.core.maths.Matrix;
import com.jge.core.maths.Vector2d;
import com.jge.core.objects.Texture;
import com.jge.jogl.beans.OpenGLMappings;
import com.jge.jogl.core.managers.GpuProgramManagerOpenGL;
import com.jge.jogl.core.managers.HardwareBufferManagerOpenGL;
import com.jge.jogl.core.managers.SceneManagerOpenGL;
import com.jge.jogl.core.managers.TextureManagerOpenGL;
import com.jge.jogl.core.objects.TextureOpenGL;
import com.jogamp.common.nio.Buffers;

public class RenderSystemOpenGL extends RenderSystem {
    private RenderWindowOpenGL rwOpenGl;

    public RenderSystemOpenGL() {
        super();
        RenderSystem.singleton = this;
    }

    public RenderWindowOpenGL getRenderWindowImpl() {
        return rwOpenGl;
    }

    @Override
    public RenderWindow createRenderWindow(String name, int width, int height) {
        if (getCurrentRenderWindow() == null) {
            rwOpenGl = new RenderWindowOpenGL();
            renderWindow = rwOpenGl;
            renderWindow.create(name, width, height);
        }
        return renderWindow;
    }

    public GL getGL() {
        return getRenderWindowImpl().getGLCanvas().getGL();
    }

    public GL2 getGL2() {
        return (GL2)getGL();
    }

    @Override
    public void beginFrame() throws JavaGraphicsException {
        super.beginFrame();
        GL2 gl = getGL2();
        gl.glPushMatrix();
        if (SceneManager.getInstance().isCameraCreated()) {
            //Camera camera = SceneManager.getInstance().createOrRetriveCamera();
            //gl.glTranslatef(camera.getX(), camera.getY(), 0.0f);
        }

    }

    @Override
    public void endFrame() throws JavaGraphicsException {
        GL2 gl = getGL2();
        gl.glPopMatrix();
        super.endFrame();
    }

    public void bindTextureOpenGL(TextureOpenGL texture) {		
        GL2 gl = getGL2();
        gl.glEnable(GL2.GL_TEXTURE_2D);

        if (texture.isLoaded()) {
            texture.getOpenGLTexture().bind(gl);
        }
    }

    public void unbindTextureOpenGL(TextureOpenGL texture) {
        GL2 gl = getGL2();
        gl.glDisable(GL2.GL_TEXTURE_2D);
    }

    @Override
    public void bindTexture(Texture texture) {
        bindTextureOpenGL((TextureOpenGL) texture);
    }

    @Override
    public void unbindTexture(Texture texture) {
        unbindTextureOpenGL((TextureOpenGL) texture);
    }

    @Override
    public void shutdown() {

    }

    @Override
    public void setRotate(float angle) {
        GL2 gl = getGL2();
        gl.glRotatef(angle, 0, 0, 1);

    }

    @Override
    public void setPosition(Vector2d position) {
        GL2 gl = getGL2();
        gl.glTranslated(position.getX(), position.getY(), 0);
    }

    @Override
    public void setWorldTransform(Matrix matrix) {
        GL2 gl = getGL2();
        gl.glMatrixMode(GL2.GL_MODELVIEW);
        //gl.glLoadIdentity();
        gl.glLoadMatrixf(matrix.getData(), 0);
    }

    @Override
    public void clearScene() {
        getGL().glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT);
        getGL().glClearColor(background[0], background[1], background[2], 1.0f);
    }

    public GLProfile getGLProfile() {
        return getRenderWindowImpl().getGLCanvas().getGLProfile();
    }

    @Override
    public void bindHardwareVertexBuffer(HardwareVertexBuffer vertexBuffer)
            throws JavaGraphicsException {
        GL2 gl = getGL2();
        HardwareVertexBufferOpenGL buffer = (HardwareVertexBufferOpenGL) vertexBuffer;



        gl.glEnableClientState(GL2.GL_VERTEX_ARRAY);
        gl.glBindBuffer(GL2.GL_ARRAY_BUFFER, buffer.getBufferId()[0]);
        gl.glVertexPointer(2 , GL.GL_FLOAT, 0, 0);

        gl.glDrawArrays(GL.GL_TRIANGLES, 0, buffer.getNumVertices());
        gl.glDisableClientState(GL2.GL_VERTEX_ARRAY);  
    }

    protected void bindVertexElementToGpu(VertexElement element, HardwareVertexBuffer vertexBuffer) {

        getGL2().glBindBuffer( GL.GL_ARRAY_BUFFER, ((HardwareVertexBufferOpenGL)vertexBuffer).getBufferId()[0] );
        switch (element.getSemantic()) {
        case VES_POSITION:
            getGL2().glVertexPointer(
                    element.getType().getCount(),
                    OpenGLMappings.map(element.getType()),
                    vertexBuffer.getVertexSize(),
                    element.getOffset());
            getGL2().glEnableClientState( GL2.GL_VERTEX_ARRAY );
            break;
        case VES_DIFFUSE:
            getGL2().glColorPointer(
                    element.getType().getCount(),
                    OpenGLMappings.map(element.getType()),
                    vertexBuffer.getVertexSize(),
                    element.getOffset());
            getGL2().glEnableClientState( GL2.GL_COLOR_ARRAY );
            break;
        case VES_TEXTURE_COORDINATES:
            getGL2().glClientActiveTexture(GL2.GL_TEXTURE0 + element.getIndex());
            getGL2().glTexCoordPointer(
                    element.getType().getCount(),
                    OpenGLMappings.map(element.getType()),
                    vertexBuffer.getVertexSize(),
                    element.getOffset());
            getGL2().glEnableClientState( GL2.GL_TEXTURE_COORD_ARRAY );
            break;
        }
    }

    @Override
    public void renderImpl(RenderOperation ro) throws JavaGraphicsException {
        //super.render(ro);

        GL2 gl2 = getGL2();

        for (VertexElement element : ro.getVertexData().getVertexDeclaration().getElementList()) {
            // TODO Сделать проверки на NullPointer
            HardwareVertexBuffer buffer = ro.getVertexData().getVertexBufferBinding().getBuffer(element.getSource());

            if (buffer != null) {
                bindVertexElementToGpu(element, buffer);
            }

        }

        //gl2.glBindBuffer( GL.GL_ARRAY_BUFFER, ((HardwareVertexBufferOpenGL)ro.getVertexData().getBuffer()).getBufferId()[0] );
        gl2.glPolygonMode( GL.GL_FRONT, GL2.GL_FILL ); // TODO сделать программно, а не напрямую
        gl2.glDrawArrays( OpenGLMappings.map(ro.getType()), 0, ro.getVertexData().getVertexCount() );

        gl2.glDisableClientState( GL2.GL_TEXTURE_COORD_ARRAY );
        gl2.glDisableClientState( GL2.GL_VERTEX_ARRAY );
        gl2.glDisableClientState( GL2.GL_COLOR_ARRAY );

    }

    @Override
    public void bindGpuProgram(GpuProgram program) throws JavaGraphicsException {
        GpuProgramOpenGL glProgram = (GpuProgramOpenGL)program;

        // gl.glUseProgram
        GL2 gl = getGL2();
        gl.glUseProgram(glProgram.getIdShaderProgram());
    }

    @Override
    public void unbindGpuProgram(GpuProgram program)
            throws JavaGraphicsException {
        GpuProgramOpenGL glProgram = (GpuProgramOpenGL)program;

        GL2 gl = getGL2();
        gl.glUseProgram(0);

    }

    @Override
    public void initializeImpl() throws JavaGraphicsException {
        setResourceGroupManager(new ResourceGroupManager(new ThreadedLoadingQueue()));
        setSceneManager(new SceneManagerOpenGL(this));
        setTextureManager(new TextureManagerOpenGL()); 
        setHardwareBufferManager(new HardwareBufferManagerOpenGL((RenderSystemOpenGL)this));
        setMaterialManager(new MaterialManager());
        setGpuProgramManager(new GpuProgramManagerOpenGL());
    }

    @Override
    public void enableTexture(boolean flag) throws JavaGraphicsException {
        GL2 gl = getGL2();
        if (flag)
            gl.glEnable(GL2.GL_TEXTURE_2D);
        else
            gl.glDisable(GL2.GL_TEXTURE_2D);
    }

    @Override
    public void beginViewPort() throws JavaGraphicsException {
        GL2 gl = getGL2();
        gl.glPushMatrix();
    }

    @Override
    public void endViewPort() throws JavaGraphicsException {
        GL2 gl = getGL2();
        gl.glPopMatrix();
    }

}