package com.jge.core.shaders;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.jge.core.beans.GpuProgramUsage;
import com.jge.core.exceptions.JavaGraphicsException;
import com.jge.core.interfaces.GpuProgram.Type;

public class Pass {
    private Technique parentTechnique;
    private GpuProgramUsage vertexProgramUsage;
    private GpuProgramUsage pixelProgramUsage;
    private Map<String, GpuProgramParameter> parameters;

    public Pass(Technique parentTechnique) throws JavaGraphicsException {
        super();
        this.parentTechnique = parentTechnique;
    }

    public void prepare() throws JavaGraphicsException {

    }

    public void load() throws JavaGraphicsException {
        if (vertexProgramUsage != null) {
            vertexProgramUsage.load();
        }

        if (pixelProgramUsage != null) {
            pixelProgramUsage.load();
        }
    }

    public void unload() throws JavaGraphicsException {
        if (pixelProgramUsage != null) {
            pixelProgramUsage.load();
        }

        if (vertexProgramUsage != null) {
            vertexProgramUsage.load();
        }
    }

    public Technique getParentTechnique() {
        return parentTechnique;
    }

    public void setVertexProgram(String name) throws JavaGraphicsException {
        if (vertexProgramUsage == null) {
            vertexProgramUsage = new GpuProgramUsage(Type.GPT_VERTEX_SHADER, this);
        }
        vertexProgramUsage.setProgram(name);
    }

    public void setPixelProgram(String name) throws JavaGraphicsException {
        if (pixelProgramUsage == null) {
            pixelProgramUsage = new GpuProgramUsage(Type.GPT_PIXEL_SHADER, this);
        }
        pixelProgramUsage.setProgram(name);
    }

    public GpuProgramUsage getVertexProgramUsage() {
        return vertexProgramUsage;
    }

    public GpuProgramUsage getPixelProgramUsage() {
        return pixelProgramUsage;
    }

    public GpuProgramParameter getParameter(String name)  {
        if (parameters == null) {
            parameters = new HashMap<String, GpuProgramParameter>();
        }
        return parameters.get(name);
    }

    public boolean hasParameters() {
        return parameters == null ? false : parameters.size()>0;
    }

    public GpuProgramParameter createParameter(String name, GpuProgramParameter.Type type) throws JavaGraphicsException {
        if (getParameter(name) != null) {
            // TODO exception
        }
        GpuProgramParameter param = new GpuProgramParameter(name, type, null);
        parameters.put(name, param);
        return param;		
    }

    public Collection<GpuProgramParameter> getParameters() throws JavaGraphicsException {
        return parameters != null ? parameters.values() : null;
    }
}
