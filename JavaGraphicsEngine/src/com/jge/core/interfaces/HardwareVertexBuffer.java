package com.jge.core.interfaces;

import com.jge.core.managers.HardwareBufferManager;

public abstract class HardwareVertexBuffer extends HardwareBuffer {
    protected HardwareBufferManager creator;
    protected int vertexSize;
    protected int numVertices;

    protected HardwareVertexBuffer(HardwareBufferManager manager, int vertexSize, int numVertices, HardwareBuffer.Usage usage) {
        super(usage);
        this.creator = manager;
        this.vertexSize = vertexSize;
        this.numVertices = numVertices;

        this.sizeInBytes = this.vertexSize * this.numVertices;
    }

    public HardwareBufferManager getCreator() {
        return creator;
    }

    public int getVertexSize() {
        return vertexSize;
    }

    public int getNumVertices() {
        return numVertices;
    }
}
