package com.jge.core.interfaces;

import com.jge.core.exceptions.JavaGraphicsException;
import com.jge.core.managers.ResourceGroupManager;
import com.jge.core.managers.SceneManager;

public abstract class RenderWindow extends RenderTarget {
    private RenderWindowListener renderWindowListener;

    public RenderWindowListener getRenderWindowListener() {
        return renderWindowListener;
    }
    public void setRenderWindowListener(RenderWindowListener renderWindowListener) {
        this.renderWindowListener = renderWindowListener;
    }

    public void destroy() throws JavaGraphicsException {
        SceneManager.getInstance().destroyScene();
    }

    abstract public void create(String name, int width, int height);
    abstract public void resize(int width, int height);
    abstract public void windowResized();
    abstract public void swapBuffers(boolean flag);	
}
