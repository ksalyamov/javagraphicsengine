package com.jge.core.managers;

import java.util.ArrayList;
import java.util.List;

import com.jge.core.beans.VertexBufferBinding;
import com.jge.core.beans.VertexDeclaration;
import com.jge.core.enums.ExceptionType;
import com.jge.core.exceptions.JavaGraphicsException;
import com.jge.core.interfaces.HardwareBuffer;
import com.jge.core.interfaces.HardwareBuffer.Usage;
import com.jge.core.interfaces.HardwareIndexBuffer;
import com.jge.core.interfaces.HardwareVertexBuffer;

public abstract class HardwareBufferManager {
    private static HardwareBufferManager instance;
    private List<VertexDeclaration> vertexDeclarations;
    private List<VertexBufferBinding> vertexBufferBindings;
    private List<HardwareVertexBuffer> vertexBuffers;
    private List<HardwareIndexBuffer> indexBuffers;

    protected HardwareBufferManager() {
        super();
        this.vertexBuffers = new ArrayList<HardwareVertexBuffer>();
        this.indexBuffers = new ArrayList<HardwareIndexBuffer>();
        this.vertexBufferBindings = new ArrayList<>();
        this.vertexDeclarations = new ArrayList<>();
    }

    public static void setSingleton(HardwareBufferManager manager) throws JavaGraphicsException {
        HardwareBufferManager.instance = manager;
    }

    public static HardwareBufferManager getInstance() {
        return HardwareBufferManager.instance;
    }

    public HardwareVertexBuffer createVertexBuffer(int vertexSize, int numVertices, HardwareBuffer.Usage usage) throws JavaGraphicsException {
        HardwareVertexBuffer buffer = createVertexBufferImpl(vertexSize, numVertices, usage);
        if (buffer != null) {
            vertexBuffers.add(buffer);
        }
        return buffer;
    }

    public VertexDeclaration createVertexDeclaration() throws JavaGraphicsException {
        VertexDeclaration decl = createVertexDeclarationImpl();
        if (decl != null) {
            vertexDeclarations.add(decl);
        }
        return decl;
    }

    public VertexBufferBinding createVertexBufferBindings() throws JavaGraphicsException {
        return new VertexBufferBinding();
    }

    abstract protected VertexDeclaration createVertexDeclarationImpl() throws JavaGraphicsException;

    abstract protected HardwareVertexBuffer createVertexBufferImpl(int vertexSize,
            int numVertices, Usage usage) throws JavaGraphicsException;

    public void destroyVertexBuffer(HardwareVertexBuffer buffer) throws JavaGraphicsException {
        throw new JavaGraphicsException(ExceptionType.NOT_IMPLEMENTED, "Method is not implemented");
    }

    public void destroyAllVertexBuffers() throws JavaGraphicsException {
        for (HardwareVertexBuffer buffer : vertexBuffers) {
            destroyVertexBuffer(buffer);
        }
    }

    public void destroyAllBuffers() throws JavaGraphicsException {
        destroyAllVertexBuffers();
    }
}
