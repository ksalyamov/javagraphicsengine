package com.jge.core.exceptions;

import com.jge.core.enums.ExceptionType;

public class JavaGraphicsException extends Exception {
    ExceptionType type;

    public JavaGraphicsException() {
        super();
        // TODO Auto-generated constructor stub
    }

    public JavaGraphicsException(String arg0, Throwable arg1, boolean arg2,
            boolean arg3) {
        super(arg0, arg1, arg2, arg3);
        // TODO Auto-generated constructor stub
    }

    public JavaGraphicsException(ExceptionType type, String arg0, Throwable arg1) {
        super(arg0, arg1);
        this.type = type;
    }

    public JavaGraphicsException(String arg0) {
        super(arg0);
        this.type = ExceptionType.DEFAULT;
    }

    public JavaGraphicsException(ExceptionType type, String arg0) {
        super(arg0);
        this.type = type;
    }

    public JavaGraphicsException(ExceptionType type, Throwable arg0) {
        super(arg0);
        this.type = type;
    }

    public JavaGraphicsException(Throwable arg0) {
        super(arg0);
    }

    public boolean isFatal() {
        return type.isFatal();
    }

    public ExceptionType getType() {
        return type;
    }
}
