package com.jge.core.interfaces;

import com.jge.core.exceptions.JavaGraphicsException;

public interface RenderWindowListener {
    /**
     * Загрузка и инициализация объектов на рендеринг. Вызывается на самом первом этапе.
     * @throws JavaGraphicsException  Внутренняя ошибка
     */
    void createScene() throws JavaGraphicsException;

    /**
     * Обновление сцены на рендеринг
     * @param elapsedTime миллисекунды, время двумя кадрами. Нужно для синхронизации по времени
     * @throws JavaGraphicsException  Внутренняя ошибка
     */
    void updateScene(float elapsedTime) throws JavaGraphicsException;

    /**
     * Вызывается при уничтожении (закрытии) окна/приложения. В методе должно происходить уничножение 
     * ресурсов, если это необходимо
     * @throws JavaGraphicsException Внутренняя ошибка
     */
    void destroyScene() throws JavaGraphicsException;
}
