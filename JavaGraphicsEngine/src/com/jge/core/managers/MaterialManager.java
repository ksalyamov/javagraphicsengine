package com.jge.core.managers;

import com.jge.core.beans.Resource;
import com.jge.core.enums.ResourceGroupName;
import com.jge.core.exceptions.JavaGraphicsException;
import com.jge.core.shaders.Material;

public class MaterialManager extends ResourceManager {
    private static MaterialManager instance;

    protected static void setInstance(MaterialManager manager) throws JavaGraphicsException {
        MaterialManager.instance = manager;
    }

    public static MaterialManager getInstance() throws JavaGraphicsException {
        return MaterialManager.instance;
    }

    public MaterialManager() throws JavaGraphicsException {
        super(ResourceGroupName.MATERIALS.getName());
        MaterialManager.setInstance(this);
    }

    @Override
    protected Resource createImpl(ResourceManager creator, String name,
            String group) throws JavaGraphicsException {
        return new Material(creator, name, group);
    }

}
