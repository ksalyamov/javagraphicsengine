package com.jge.core.beans;

import com.jge.core.enums.LoadingStatus;
import com.jge.core.exceptions.JavaGraphicsException;
import com.jge.core.interfaces.TaskItem;

public class Task {
    private TaskItem taskItem;
    private boolean isBackgroundLoading;

    public boolean isBackgroundLoading() {
        return isBackgroundLoading;
    }

    public void setBackgroundLoading(boolean isBackgroundLoading) {
        this.isBackgroundLoading = isBackgroundLoading;
    }

    public Task(TaskItem taskItem) {
        super();
        this.taskItem = taskItem;
    }

    public void preparing() throws JavaGraphicsException {
        taskItem.setStatus(LoadingStatus.PREPARING);
        try {
            taskItem.preparing();
            taskItem.setStatus(LoadingStatus.PREPARED);
        } catch (JavaGraphicsException e) {
            taskItem.setStatus(LoadingStatus.UNLOADED);
            throw new JavaGraphicsException(e.getType(), e);
        }
    }

    public void loading() throws JavaGraphicsException {
        try {
            taskItem.loading();
            taskItem.setStatus(LoadingStatus.LOADED);
        } catch (JavaGraphicsException e) {
            taskItem.setStatus(LoadingStatus.UNLOADED);
            throw new JavaGraphicsException(e.getType(), e);
        }
    }

    public TaskItem getTaskItem() {
        return taskItem;
    }

}
