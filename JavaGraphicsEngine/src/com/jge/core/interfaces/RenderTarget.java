package com.jge.core.interfaces;

import java.util.ArrayList;
import java.util.List;

import com.jge.core.beans.Camera;
import com.jge.core.beans.Viewport;
import com.jge.core.exceptions.JavaGraphicsException;

public abstract class RenderTarget {
    private String name;
    private int width;
    private int height;
    private boolean active;
    private List<Viewport> viewports;
    
    public RenderTarget() {
        super();
        this.viewports = new ArrayList<Viewport>();
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public int getWidth() {
        return width;
    }
    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }
    public void setHeight(int height) {
        this.height = height;
    }

    public boolean isActive() {
        return active;
    }
    public void setActive(boolean active) {
        this.active = active;
    }

    abstract public void beginUpdate();
    abstract public void updateViewport();
    abstract public void endUpdate();
    
    public Viewport addViewport(Camera camera) {
        Viewport viewport = new Viewport(camera, this);
        viewports.add(viewport);
        return viewport;
    }
    
    public void update() throws JavaGraphicsException {
        for (Viewport viewport : viewports) {
            viewport.update();
        }
    }
}
