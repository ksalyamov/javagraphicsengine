package com.jge.core.enums;

public enum ExceptionType {
    DEFAULT(false),
    DUPLICATED_ITEM(true),
    NOT_IMPLEMENTED(true),
    FILE_NOT_FOUND(true),
    BAD_FILE_FORMAT(true),
    THREAD_INTERRUPTED(true),
    MISSED_DATA(true);

    boolean fatal;
    ExceptionType(boolean fatal) {
        this.fatal = fatal;
    }

    public boolean isFatal() {
        return this.fatal;
    }
}
