package com.jge.core.beans;

import java.io.File;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GroupLocation {
    private File path;
    private String type;
    private Map<String, File> files;

    protected GroupLocation() {
        super();
    }

    public GroupLocation(File file, String type) {
        this();
        this.path = file;
        this.type = type;
        this.files = new HashMap<String, File>();
        loadLocation();
    }

    public GroupLocation(String file, String type) {
        this(new File(file), type);
    }

    /**
     * Загружает список файлов по указанному пути и типу данных
     */
    public void loadLocation() {
        for (String fileName : path.list()) {
            if (fileName.endsWith(type))
                files.put(fileName, new File(path, fileName));
        }
    }

    public File getPath() {
        return path;
    }

    public String getType() {
        return type;
    }

    public File getFile(String name) {
        return files.get(name);
    }

    public Collection<File> getFiles() {
        return files.values();
    }
}
