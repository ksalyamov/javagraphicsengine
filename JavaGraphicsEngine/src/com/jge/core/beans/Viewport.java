package com.jge.core.beans;

import com.jge.core.exceptions.JavaGraphicsException;
import com.jge.core.interfaces.RenderTarget;

public class Viewport {
    private Camera camera;
    private RenderTarget target;
    
    public Viewport(Camera camera, RenderTarget target) {
        super();
        this.camera = camera;
        this.target = target;
    }
    
    public void update() throws JavaGraphicsException {
        /*target.beginUpdate();
        target.updateViewport();
        target.endUpdate();*/
        if (camera != null) {
            camera.renderScene(this);
        }
    }
}
